$(document).ready(function(){
    $('#register_form').validate({

      rules:{

        fname:{
          required: true
        },
        lname:{
          required: true
        },
        email:{
          required: true,
          email: true
        },
        password:{
          required: true,
          minlength: 8,
          maxlength: 24
        },
        password2:{
          required: true,
          equalTo: '#rpw'
        }    

      },

      messages:{

        fname:{
            required: 'Firstname Required'
        },
        lname:{
            required: 'Lastname Required'
        },
        email:{
          required: 'Email Required'
        },
        password:{
          required: 'Nominate Password'
        },
        password2:{
          required: 'Retype Password'
        },
      }

    });

    $('#login_form').validate({

      rules:{
        email:{
          required: true,
          email: true
        },
        password:{
          required: true,
          minlength: 8
        },
      },

      messages:{

        email:{
          required: 'Enter your email'
        },
        password:{
          required: 'Enter your password'
        },
      }
    });
  });
  