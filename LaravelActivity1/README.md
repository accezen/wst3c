## Routes

1. **/home** - Home Page
2. **/about** - About Page
2. **/gallery** - Gallery/Contact Page
4. **/login** - Login Page
5. **/register** - Registration Page


## Files and Folders Updated Added

1. **./resources/views** Folder

	1. **index.blade.php**.
	2. **home.blade.php**.
	2. **about.blade.php**.
	4. **gallery.blade.php**.
	5. **register.blade.php**  
	
2. **./public/css** Folder
3. **./public/js** Folder
4. **./public/images** Folder
5. **./routes/web.php** Folder


---



