<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="{{ url('../images/favicon.png') }}">
    <title>Register</title>
    <link rel="stylesheet" href="{{ ('../css/style2.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    
</head>
<body>
    <div class="mainContainer">
        <div class="container-fluid">
            <div class="row inner-wrapper r_label pt-5">
                <p class="logoHeader">Register</p>
                <p class="subHeader">Fill in the Form to create an account</p>
                <br>
                <div class="row">
                    <form class="frm-wrapper" id="register_form" action="{{ url ('/home') }}" method="get">
        
                        <div class="container col-sm-5 col-sm-offset-3">
                            <div class="row">
                                <div class="col-sm-7">
                                    <label id="f-labell" for="fname">Firstname</label>
                                    <input class="input-boxx mb-2" type="text" name="fname">
                                </div>
                                <div class="col-sm-5">
                                    <label id="f-labell" for="lname">Lastname</label>
                                    <input class="input-boxx mb-2" type="text" name="lname">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label id="f-labell" for="email">Email</label>
                                    <input class="input-boxx mb-2" type="email" name="email">
                                </div>
                                <div class="col-sm-12">
                                    <label id="f-labell" for="password">Password</label>
                                    <input class="input-boxx mb-2" type="password" name="password" id="rpw">
                                </div>
                                <div class="col-sm-12">
                                    <label id="f-labell" for="password">Confirm Password</label>
                                    <input class="input-boxx mb-2" type="password" name="password2" id="rpw2">
                                </div>
                            </div>
                            <div class="row mt-3 ">
                                <center>
                                <div class="col-sm-12">
                                    <input type="submit" class="subb" value="Register" id="submit">
                                </div>
                                <div class="opt">
                                    <p>Have an Account?</p>
                                </div>
        
                                <div class="fg pb-3">
                                    <a class="su" href="{{ ('/login') }}">Login</a>
                                </div>
                                </center>
                                
                            </div>
                        </div>

                        
                    </form>
                </div>
                
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" ></script>
    <script src="./js/validate.js"></script>
</body>
</html>