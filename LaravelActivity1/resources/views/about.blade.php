<!DOCTYPE HTML>
<html>

<head>
    <link rel="icon" type="image/x-icon" href="{{ url('../images/favicon.png') }}">
    <title>Jan Patrick Urbano | About</title>
    <link href="{{ url('../css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ url('../js/jquery.min.js') }}"></script>
    <!-- Custom Theme files -->
    <link href="{{ url('../css/style.css') }}" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- webfonts -->
    <link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <!-- webfonts -->
    <!---- start-smoth-scrolling---->
    <script type="text/javascript" src="{{ url('../js/move-top.js') }}"></script>
    <script type="text/javascript" src="{{ url('../js/easing.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
            });
        });
    </script>
    <!---- start-smoth-scrolling---->
</head>

<body>
    <!-- container -->
    <!-- header -->
    <div id="main" class="header">
        <div class="container-fluid">
            <!-- top-hedader -->
            <div class="row top-header">
                <!-- /logo -->
                <!--top-nav---->
                <div class="logo">
                    <h1><a href="home.html"><span>J</span>PATRICK</a></h1>
                </div>
                <div class="navigation-right">
                    <span class="menu"><img src="images/menu.png" alt=" " /></span>
                    <nav class="link-effect-3" id="link-effect-3">
                        <ul class="nav1 nav nav-wil">
                            <li><a href="{{ ('/home') }}" data-hover="HOME">HOME</a></li>
                            <li class="active"><a data-hover="ABOUT" href="{{ ('/about') }}">ABOUT</a></li>
                            <li><a  data-hover="GALLERY" href="{{ ('/gallery') }}">GALLERY</a></li>
                            <li><a  data-hover="LOGIN" href="{{ ('/index') }}">LOGIN</a></li>
                        </ul>
                    </nav>
                    <!-- script-for-menu -->
                    <script>
                        $("span.menu").click(function () {
                            $("ul.nav1").slideToggle(300, function () {
                                // Animation complete.
                            });
                        });
                    </script>
                    <!-- /script-for-menu -->
                </div>

            </div>
            <!-- /top-hedader -->
        </div>
    </div>
    <div class="container-fluid m-5">
        <div class="row">
             <!-- about -->
        <div id="about" class="about">
            <div class="abt_container">
            <div class="col-md-6 about-left">
                <div id="owl-demo1" class="owl-carousel owl-carousel2">
                    <div class="item">
                        <div class="about-left-grid">
                            <h2>
                                <font face="Montserrat">Hi! I'm <span>JAN PATRICK</font></span>
                            </h2>
                            <p>
                                <font face="Montserrat">I’m currently fulfilling my degree at Pangasinan State
                                    University, taking up Bachelor of Science in Information technology.
                                    <br>
                                    I describe myself being a person focused on achieving my goals and objectives ,
                                    I always look forward towards my dream and fulfilling it.
                                    We only live once, live our life to the fullest, and make everything fair for us
                                    using hard work.
                                </font>
                            </p>
                            <ul>
                                <li><a href="{{ ('/gallery') }}">My Gallery</a></li>
                                <li><a href="{{ ('/gallery') }}">Get in Touch</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="about-left-grid">
                            <h2>
                                <font face="Montserrat">My <span>Life Philosophy </font></span>
                            </h2>
                            <br>
                            <p>
                                <font face=" Montserrat Thin" size="100">"Its in your hand to <br> make everything
                                    fair for you"</font>
                            </p>
                            <ul>
                                <li><a href="{{ ('/gallery') }}">My Gallery</a></li>
                                <li><a href="{{ ('/gallery') }}">Get in Touch</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="about-left-grid">
                            <h2>
                                <font face="Montserrat">Hi! I'm <span>JAN PATRICK</font></span>
                            </h2>
                            <p>
                                <font face="Montserrat">
                                    Defining ourselves is one of the hardest thing to do, it really take us time for
                                    us to get the real definition of ourselves as it change from time to time and it
                                    may vary on person whom we are dealing with.
                                    <br>
                                    <br>
                                    I can say my personality Is like a mirror, I can be a person if you will be a
                                    person to me, but if you face the mirror such a negative way, expect the same in
                                    return.
                                    <br>
                                </font>
                            </p>
                            <ul>
                                <li><a href="{{ ('/gallery') }}">My Gallery</a></li>
                                <li><a href="{{ ('/gallery') }}">Get in Touch</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-md-6 about-right">

            </div>

            <div class="vitae mt-3">
                <div class="col-md-12 data">
                    <h6><font face="Montserrat" color="white" size="200">ABOUT</font></h6>
					
					<ul class="address">
					    <li><font face="Montserrat">
							<ul class="address-text">
								<li><b>Name</b></li>
								<li>Jan Patrick Urbano</li>
							</ul>
						</font></li>

						<li><font face="Montserrat">
							<ul class="address-text">
								<li><b>Birthdate</b></li>
								<li>January 11 2001</li>
							</ul>
						</font></li
                        >
						<li><font face="Montserrat">
							<ul class="address-text">
								<li><b>Age</b></li>
								<li>21</li>
							</ul>
						</font></li>

						<li><font face="Montserrat">
							<ul class="address-text">
								<li><b>Phone </b></li>
								<li>+639661362302</li>
							</ul>
						</font></li>
						<li><font face="Montserrat">
							<ul class="address-text">
								<li><b>Address </b></li>
								<li>#1 Purok Magsaysay, San Juan, Moncada, Tarlac</li>
							</ul>
						</font></li>
						<li><font face="Montserrat">
							<ul class="address-text">
								<li><b>E-mail </b></li>
								<li><a href="mailto:example@mail.com"> janpatrickdelacruzurbano@gmail.com</a></li>
							</ul>
						</font></li>
						
					</ul>
				</div>
                </div>>
                <section class="ftco-section" id="skills-section">
                    <div class="container">
                        <div class="row justify-content-center pb-5">
                  <div class="col-md-12 heading-section text-center ftco-animate fadeInUp ftco-animated">
                    <h2 class="mb-4">Knowledge Development</h2>
                    <p></p>
                  </div>
                </div>
                        <div class="row">
                            <div class="col-md-6 animate-box">
                                <div class="progress-wrap ftco-animate fadeInUp ftco-animated">
                                    <h3>HTML5</h3>
                                    <div class="progress">
                                         <div class="progress-bar color-1" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                        <span>50%</span>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 animate-box">
                                <div class="progress-wrap ftco-animate fadeInUp ftco-animated">
                                    <h3>CSS3</h3>
                                    <div class="progress">
                                         <div class="progress-bar color-2" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                        <span>50%</span>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 animate-box">
                                <div class="progress-wrap ftco-animate fadeInUp ftco-animated">
                                    <h3>JavaScript</h3>
                                    <div class="progress">
                                         <div class="progress-bar color-3" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:10%">
                                        <span>10%</span>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 animate-box">
                                <div class="progress-wrap ftco-animate fadeInUp ftco-animated">
                                    <h3>JQuery</h3>
                                    <div class="progress">
                                         <div class="progress-bar color-4" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:10%">
                                        <span>10%</span>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 animate-box">
                                <div class="progress-wrap ftco-animate fadeInUp ftco-animated">
                                    <h3>Laravel</h3>
                                    <div class="progress">
                                         <div class="progress-bar color-5" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:10%">
                                        <span>10%</span>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 animate-box">
                                <div class="progress-wrap ftco-animate fadeInUp ftco-animated">
                                    <h3>WordPress</h3>
                                    <div class="progress">
                                         <div class="progress-bar color-6" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width:5%">
                                        <span>5%</span>
                                          </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
               
            </div>
            
            
                
                <link href="css/owl.carousel.css" rel="stylesheet">
                <script src="js/owl.carousel.js"></script>
                <script>
                    $(document).ready(function () {
                        $("#owl-demo1").owlCarousel({
                            items: 1,
                            lazyLoad: false,
                            autoPlay: true,
                            navigation: false,
                            navigationText: false,
                            pagination: true,
                        });
                    });
                </script>
                <!-- Feedback -->
                <script>
                    $(document).ready(function () {
                        $("#owl-demo3").owlCarousel({
                            items: 1,
                            lazyLoad: false,
                            autoPlay: true,
                            navigation: false,
                            navigationText: true,
                            pagination: true,
                        });
                    });
                </script>
            </div>
            <!-- /about -->
        </div>
        <!-- services -->
			<div id="services" class="services">
				<div class="container">
					<div class="service-head one text-center ">
						<h4>WHAT I DO</h4>
						<h3>MY <span>HOBBIES</span></h3>
						<span class="border two"></span>
					</div>
					<!-- services-grids -->
					<div class="wthree_about_right_grids w3l-agile">
				<div class="col-md-6 wthree_about_right_grid">
					<div class="col-xs-4 wthree_about_right_grid_left">
						<div class="hvr-rectangle-in">
							<i class="glyphicon glyphicon-pencil"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_about_right_grid_right">
						<h4><font face="Montserrat Thin">CODING</font></h4>
						<p><font face="montserrat">
						It is called "Passion" when you don't get tired on doing something maybe it is hard, but if you are passionate on what you are doing you can do it with ease, like coding, considering the fact that it's not easy to understand and requires some logical thinking skills.</font></p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree_about_right_grid">
					<div class="col-xs-4 wthree_about_right_grid_left">
						<div class="hvr-rectangle-in">
							<i class="glyphicon glyphicon-cog"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_about_right_grid_right">
						<h4><font face="Montserrat Thin">READING</font></h4>
						<p><font face="montserrat">I am fond of reading books, PDF, and other instructional materials, maybe majority find reading boring, as modern technology can offer more entertaining activities, such as mobile application(games, social networking). Dont forget to read is to learn, many learning can be obtain via reading.</font></p>

					</div>
					<div class="clearfix"> </div>
				</div>
				
                <div class="col-md-6 wthree_about_right_grid">
					<div class="col-xs-4 wthree_about_right_grid_left">
						<div class="hvr-rectangle-in">
							<i class="glyphicon glyphicon-leaf"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_about_right_grid_right">
						<h4><font face="Montserrat Thin">WRITING</font></h4>
						<p><font face="montserrat">Im not that good at writing, but honestly, I loved doing it, as a former staff of our school publication during my secondary level, I experienced writing feature articles, news articles etc.</font></p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree_about_right_grid">
					<div class="col-xs-4 wthree_about_right_grid_left">
						<div class="hvr-rectangle-in">
							<i class="glyphicon glyphicon-gift"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_about_right_grid_right">
						<h4><font face="Montserrat Thin">TRAVELING</font></h4>
						<p><font face="montserrat" >This is one of the most satisfying activity that I want to do always, I want to do traveling as this serves as an avenue for me to skip from a stress and unwind.</font></p>
					</div>
				</div>
			</div>

					<!-- services-grids -->
				</div>
			</div>
            <footer class = "end">
                <div class="copy_right text-center">
                  <p class="foot">© 2022 Personal Website . All rights reserved | Inspired by W3Layouts Design</a></p>
                   <ul class="social-icons two">
                        <li><a href="https://twitter.com" class="twitter"> </a></li>
                        <li><a href="https://web.facebook.com/Accezen" class="fb"> </a></li>
                        <li><a href="https://linkedin.com" class="in"> </a></li>
                        <li><a href="https://dribbble.com" class="dott"> </a></li>
                    </ul>
                </div>
            </footer>
    </div>
    <script src="{{ ('../js/bootstrap.js') }}"></script>
</body>

</html>