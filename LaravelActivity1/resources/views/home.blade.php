<!DOCTYPE HTML>
<html>

<head>
    <link rel="icon" type="image/x-icon" href="{{ url('../images/favicon.png') }}">
    <title>Jan Patrick Urbano | Home</title>
    <link href="{{ url('../css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <script src="{{ url('..js/jquery.min.js') }}"></script>
    <link href="{{ url('../css/style.css') }}" rel='stylesheet' type='text/css' />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <script type="text/javascript" src="{{ url('..js/move-top.js') }}"></script>
    <script type="text/javascript" src="{{ url('..js/easing.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
</head>

<body>
    <div id="main" class="header">
        <div class="container-fluid">
            <div class="row top-header">
                <div class="logo">
                    <h1><a href="{{ url('/home') }}"><span>J</span>PATRICK</a></h1>
                </div>
                <div class="navigation-right">
                    <span class="menu"><img src="images/menu.png" alt=" " /></span>
                    <nav class="link-effect-3" id="link-effect-3">
                        <ul class="nav1 nav nav-wil">
                            <li class="active"><a href="{{ url('/home') }}" data-hover="HOME">HOME</a></li>
                            <li><a data-hover="ABOUT" href="{{ url('/about') }}">ABOUT</a></li>
                            <li><a data-hover="GALLERY" href="{{ url('/gallery') }}">GALLERY</a></li>
                            <li><a data-hover="LOGIN" href="{{ url('/login') }}">LOGIN</a></li>
                        </ul>
                    </nav>
                    <script>
                        $("span.menu").click(function() {
                            $("ul.nav1").slideToggle(300, function() {
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div id="home" class="banner-info">
                <div class="col-md-7 header-right mt-5">
                    <h1>HELLO WORLD!</h1>
                    <h6 style="font-family: Montserrat; font-size: 40px;">JAN PATRICK DELA CRUZ URBANO</h6>

                    <h4 class="mb-3" style="font-family: Montserrat; color: rgb(185, 184, 184); font-size: 28px;">Future Web Developer</h4>

                    <div class="button_container">
                        <ul>
                            <li><a class="btn btn-q" href="{{ url('/gallery') }}">Get in Touch</a></li>
                        </ul>
                    </div>

                </div>
            </div>
            <footer class="end">
                <div class="copy_right text-center">
                    <p class="foot">© 2022 Personal Website . All rights reserved | Inspired by W3Layouts Design</a></p>
                    <ul class="social-icons two">
                        <li><a href="https://twitter.com" class="twitter"> </a></li>
                        <li><a href="https://web.facebook.com/Accezen" class="fb"> </a></li>
                        <li><a href="https://linkedin.com" class="in"> </a></li>
                        <li><a href="https://dribbble.com" class="dott"> </a></li>
                    </ul>
                </div>
            </footer>
        </div>
        <link href="{{ url('../css/owl.carousel.css') }}" rel="stylesheet">
        <script src="{{ url('../js/owl.carousel.js') }}"></script>
        <script>
            $(document).ready(function() {
                $("#owl-demo1").owlCarousel({
                    items: 1,
                    lazyLoad: false,
                    autoPlay: true,
                    navigation: false,
                    navigationText: false,
                    pagination: true,
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $("#owl-demo3").owlCarousel({
                    items: 1,
                    lazyLoad: false,
                    autoPlay: true,
                    navigation: false,
                    navigationText: true,
                    pagination: true,
                });
            });
        </script>
    </div>
    </div>
    </div>
    <script src="{{ url('../js/bootstrap.js') }}"></script>
</body>

</html>