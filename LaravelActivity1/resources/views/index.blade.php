<?php

use Illuminate\Routing\RouteUri;

if(isset($_GET['#submit'])){
    view('/home');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ url('../css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ url('../css/style2.css') }}" rel='stylesheet' type='text/css' />
    <link rel="icon" type="image/x-icon" href="{{ url('../images/favicon.png') }}">
    <title>Login</title>
</head>
<body>
    <div class="mainContainer">
        <div class="container-fluid">
            <div class="row innerContainer">
                <div class="logoHeader">
                    <p class="login" >Sign in</p>
                </div>
                <center>
                <div class="formContainer col-sm-12">
                    <form id ="login_form" method="get" action="{{ url ('home') }}">
                        <div class="form-group mb-3">
                            <label id ="f-label" for="email">Email</label>
                            <input type="email" class="input-box mb-1" name="email">
                        </div>

                        <div class="form-group pww">
                            <label id="f-label" for="password">Password</label>
                            <input type="password" class="input-box mb-1" id="pw" name="password">
                        </div>

                        <div class="fg">
                            <a class="fgg" href="">Forgot Password?</a>
                        </div>

                        <button class="sub" type="submit" id="submit">Login</button>
                        <br>

                        <div class="opt">
                            <p class="or">Or</p>
                            <p>Don't have an account?</p>
                        </div>

                        <div class="fg">
                            <a class="su" href="{{ url('/register') }}">Sign Up</a>
                        </div>
                       
                
                    </form>
                </div
                <center>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" ></script>
    <script src="{{ url('../js/validate.js') }}"></script>
</body>
</html>