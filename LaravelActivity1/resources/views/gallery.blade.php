<!DOCTYPE HTML>
<html>

<head>
    <link rel="icon" type="image/x-icon" href="{{ url('../images/favicon.png') }}">
    <title>Jan Patrick Urbano | Gallery</title>
    <link href="{{ url('../css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ url('../js/jquery.min.js') }}"></script>
    <!-- Custom Theme files -->
    <link href="{{ url('../css/style.css') }}" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- webfonts -->
    <link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <!-- webfonts -->
    <!---- start-smoth-scrolling---->
    <script type="text/javascript" src="{{ url('../js/move-top.js') }}"></script>
    <script type="text/javascript" src="{{ url('../js/easing.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!---- start-smoth-scrolling---->
</head>

<body>
    <!-- container -->
    <!-- header -->
    <div id="main" class="header">
        <div class="container-fluid">
            <!-- top-hedader -->
            <div class="row top-header">
                <!-- /logo -->
                <!--top-nav---->
                <div class="logo">
                    <h1><a href="{{ ('/home') }}"><span>J</span>PATRICK</a></h1>
                </div>
                <div class="navigation-right">
                    <span class="menu"><img src="images/menu.png" alt=" " /></span>
                    <nav class="link-effect-3" id="link-effect-3">
                        <ul class="nav1 nav nav-wil">
                            <li><a href="{{ ('/home') }}" data-hover="HOME">HOME</a></li>
                            <li><a data-hover="ABOUT" href="{{ ('/about') }}">ABOUT</a></li>
                            <li class="active"><a data-hover="GALLERY" href="{{ ('/gallery') }}">GALLERY</a>
                            </li>
                            <li><a data-hover="LOGIN" href="{{ ('/login') }}">LOGIN</a></li>
                        </ul>
                    </nav>
                    <script>
                        $("span.menu").click(function() {
                            $("ul.nav1").slideToggle(300, function() {
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="portfolio" id="port">
                <div class="service-head text-center">
                    <h4>MY SNAPSHOTS</h4>
                    <h3>MY <span>GALLERY</span></h3>
                    <span class="border"></span>
                </div>
                <div class="portfolio-grids">
                    <script src="{{ url('../js/easyResponsiveTabs.js') }}" type="text/javascript"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#horizontalTab').easyResponsiveTabs({
                                type: 'default',        
                                width: 'auto', 
                                fit: true 
                            });
                        });
                    </script>
                    <div class="sap_tabs">
                        <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                            <ul class="resp-tabs-list">
                                <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>SELFIES</span>
                                </li>
                                <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>RANDOM</span></li>
                                <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>PHOTOGRAPHY</span>
                                </li>
                                <li class="resp-tab-item" aria-controls="tab_item-3" role="tab"><span>ALL</span></li>
                            </ul>


                            <div class="resp-tabs-container">
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self1.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self2.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self3.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self5.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self4.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self6.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self7.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self8.jpg" alt="">

                                        </a>

                                    </div>

                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self9.jpg" alt="">

                                        </a>

                                    </div>

                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self10.jpg" alt="">

                                        </a>

                                    </div>

                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self11.jpg" alt="">

                                        </a>

                                    </div>

                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self12.jpg" alt="">

                                        </a>

                                    </div>



                                    <!--RANDOM-->


                                    <div class="clearfix"></div>
                                </div>
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r1.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r2.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r3.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r4.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r5.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r6.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r7.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r8.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r9.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r10.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r11.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r12.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r13.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r14.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r15.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r16.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r17.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r18.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r19.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r20.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r21.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r22.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r23.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r24.jpg" alt="">
                                        </a>

                                    </div>





                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r25.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r26.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r27.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r28.jpg" alt="">
                                        </a>

                                    </div>





                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r29.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r30.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r14.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r9.jpg" alt="">
                                        </a>

                                    </div>







                                    <div class="clearfix"></div>
                                </div>
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/p1.jpg" alt="">

                                        </a>

                                    </div>


                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/p2.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/p3.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/p1.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-3">
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self1.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self2.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self3.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self5.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self4.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self6.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self7.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self8.jpg" alt="">

                                        </a>

                                    </div>

                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self9.jpg" alt="">

                                        </a>

                                    </div>

                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self10.jpg" alt="">

                                        </a>

                                    </div>

                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self11.jpg" alt="">

                                        </a>

                                    </div>

                                    <div class="col-md-3 team-gd yes_marg ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/self12.jpg" alt="">

                                        </a>

                                    </div>



                                    <!--RANDOM-->



                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r1.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r2.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r3.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r4.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r5.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r6.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r7.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r8.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r9.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r10.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r11.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r12.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r13.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r14.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r15.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r16.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r17.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r18.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r19.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r20.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r21.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r22.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r23.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r24.jpg" alt="">
                                        </a>

                                    </div>





                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r25.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r26.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r27.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r28.jpg" alt="">
                                        </a>

                                    </div>





                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r29.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r30.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r14.jpg" alt="">
                                        </a>

                                    </div>




                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/r9.jpg" alt="">
                                        </a>

                                    </div>








                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/p1.jpg" alt="">

                                        </a>

                                    </div>


                                    <div class="col-md-3 team-gd ">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/p2.jpg" alt="">

                                        </a>

                                    </div>
                                    <div class="col-md-3 team-gd">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/p3.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-3 team-gd">

                                        <a href="#" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/p1.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-section" id="site">
                <div class="container">

                    <div class="service-head text-center">
                        <h4>What's Up!</h4>
                        <h3><span>CONTACT US</span></h3>
                        <span class="border"></span>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-lg-8 mb-5">
                            <form action="#" method="get">
                                <div class="form-group row">
                                    <div class="col-md-6 mb-4 mb-lg-0">
                                        <input type="text" class="form-control" placeholder="First name" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="First name" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" placeholder="Email address" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <textarea name="" id="" class="form-control" placeholder="Write your message." cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6 mr-auto">
                                        <input type="submit" id="submit" class="bts text-white py-3 px-5" value="Send Message" required>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-4 ml-auto">
                            <div class="bg-white p-3 p-md-5">
                                <h3 class="text-black mb-4">Contact Info</h3>
                                <ul class="list-unstyled footer-link">
                                    <li class="d-block mb-3">
                                        <span class="d-block text-black">Address: </span>
                                        <span>#1 Purok Magsaysay, San Juan, Moncada, Tarlac Philippines</span>
                                    </li>
                                    <li class="d-block mb-3"><span class="d-block text-black">Phone: </span><span>+63 966-136-2302</span></li>
                                    <li class="d-block mb-3"><span class="d-block text-black">Email: </span><span>janpatrickdelacruzurbano@gmail.com</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <footer class="end">
                <div class="copy_right text-center">
                    <p class="foot">© 2022 Personal Website . All rights reserved | Inspired by W3Layouts Design</a></p>
                    <ul class="social-icons two">
                        <li><a href="https://twitter.com" class="twitter"> </a></li>
                        <li><a href="https://web.facebook.com/Accezen" class="fb"> </a></li>
                        <li><a href="https://linkedin.com" class="in"> </a></li>
                        <li><a href="https://dribbble.com" class="dott"> </a></li>
                    </ul>
                </div>
            </footer>

            <a href="gallery.html" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
            <!--start-smooth-scrolling-->
            <script type="text/javascript">
                $(document).ready(function() {

                    $().UItoTop({
                        easingType: 'easeOutQuart'
                    });

                });
            </script>
            <!--end-smooth-scrolling-->
            <!-- //for bootstrap working -->
            <script src="{{ url('../js/bootstrap.js') }}"></script>
        </div>
    </div>
    <script src="{{ url('../js/bootstrap.js') }}"></script>
</body>

</html>