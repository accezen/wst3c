<?php
session_start();
require 'vendor/autoload.php';

if (isset($_POST['userID'])) {
    $_SESSION['userID'] = $_POST['userID'];
    $_SESSION['email'] = $_POST['email'];
    $_SESSION['picture'] = $_POST['picture'];
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['accessToken'] = $_POST['accessToken'];
    exit("success");
}

if(isset($_POST['g-btn'])){
    require_once 'gconfig.php';
    $_SESSION['soc-plat'] = "Google";
    $login_link = $client -> createAuthUrl();
    header('Location:'.$login_link);
}

    $name = "Jan Patrick D.C Urbano";
    $year_sec = "BSIT 3C";
    $subject = "Elective 1 (Web System and Technologies 2";
    date_default_timezone_set('Asia/Manila');
    $date = date('F d, Y');
    $time = date('h:i a');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login with Facebook and Google API Script</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome 6 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card border-0 shadow rounded-3 my-5">
                    <div class="card-body px-3 py-4 ">
                        <h5 class="card-title text-center mb-5 ">Sign In</h5>
                        <div class="data m-0">
                            <div class="name">
                                <?php echo "Name: $name";?>
                            </div>
                            <span class="date">
                                <?php echo $date;?>
                            </span>
                            <div class="sec">
                                <?php echo "Year & Section: $year_sec";?>
                            </div>
                            <div class="time text-uppercase">
                                <?php echo $time;?>
                            </div> 
                        </div>
                        <hr class="my-4">
                        <form method = "POST">
                        <div class="d-grid mb-2">
                            <button class="btn btn-facebook btn-login fw-bold p-2" type="submit" onclick="logIn()" id="fb-btn">
                                <i class="fab fa-facebook-f me-2"></i> Login with Facebook
                            </button>
                        </div>
                        <div class="d-grid">
                            <button class="btn btn-google btn-login fw-bold p-2" name = "g-btn"  type="submit" style="width: 100%;" >
                                <i class="fab fa-google me-2"></i> Sign in with Google
                            </button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script>
        var person = { userID: "", name: "", accessToken: ""};

        function logIn() {
            FB.login(function (response) {
                if (response.status == "connected") {
                    person.userID = response.authResponse.userID;
                    person.accessToken = response.authResponse.accessToken;

                    FB.api('/me?fields=id,name,email,picture.type(large)', function (userData) {
                        person.name = userData.name;
                        

                        $.ajax({
                           url: "login.php",
                           method: "POST",
                           data: person,
                           dataType: 'text',
                           success: function (serverResponse) {
                               console.log(person);
                           }
                        });
                    });
                }
            }, {scope: 'public_profile, email'})
        }

        window.fbAsyncInit = function() {
            FB.init({
                appId            : '947167532832017',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v2.11'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</body>
</html>