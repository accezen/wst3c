<?php
session_start();
require_once 'vendor/autoload.php';


if(isset($_SESSION['soc-plat'])== "Google"){
    require_once 'gconfig.php';

    if(isset($_SESSION['access_token'])){
        $client -> setAccessToken($_SESSION['access_token']);
    }
    else if(isset($_GET['code'])){
        $token = $client ->fetchAccessTokenWithAuthCode($_GET['code']);
        $_SESSION['access_token'] = $token;
    }else{
        header('Location: login.php');
        exit();
    }
        
    $gauth = new Google_Service_Oauth2($client);
        $google_info = $gauth -> userinfo_v2_me -> get();
        $name = $google_info -> name;
        $_SESSION['user_name'] = $name;
}

$year_sec = "BSIT 3C";
$subject = "Elective 1 (Web System and Technologies 2";
date_default_timezone_set('Asia/Manila');
$date = date('F d, Y');
$time = date('h:i a');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login with Facebook and Google API Script</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome 6 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card border-0 shadow rounded-3 my-5">
                    <div class="card-body px-3 py-4 ">
                        <h5 class="card-title text-center mb-5 ">Welcome <?php echo $_SESSION['soc-plat']; ?></h5>
                        <div class="data m-0">
                            <div class="name">
                                <?php echo "Name: $name ";?>
                            </div>
                            <span class="date">
                                <?php echo $date;?>
</span>
                            <div class="sec">
                                <?php echo "Year & Section: $year_sec";?>
                            </div>
                            <div class="time text-uppercase">
                                <?php echo $time;?>
                            </div> 
                        </div>
                        <hr class="my-4">
                        <form method = "POST" action = "logout.php">
                        <div class="d-grid">
                        <button class="btn btn-danger fw-bold p-2" type="submit" style="width:100%;" >
                        Logout                        
                        </button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>