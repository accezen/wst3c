<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ValidationController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(' ', [ValidationController::class, 'showform1']);

Route::get('/login', [ValidationController::class, 'showform1']);

Route::get('/admin', [ValidationController::class, 'showform2']);

Route::get('/register', [ValidationController::class, 'showform3']);

Route::post('/login', [ValidationController::class, 'validateform1']);

Route::post('/register',[ValidationController::class, 'regvalidateform']);

Route::get('/dashboard/{id}', [ValidationController::class,'dashboard']);

Route::get('admin-dashboard/{id}', [ValidationController::class,'adminDashboard']);

Route::post('/admin/login', [ValidationController::class,'validateform2']);

Route::post('/home',[ValidationController::class,'validateform3']);

Route::get('/appointments/{id}', [ValidationController::class, 'appointments']);

Route::post('/delete', [ValidationController::class, 'deleteAppointment']);

Route::post('/approve', [ValidationController::class, 'approveAppointment']);



