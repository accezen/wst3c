<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ValidationController extends Controller
{
    public function showform1() {
        return view('login');
     }

     public function showform2() {
        return view('admin_login');
     }

     public function showform3() {
        return view('register');
     }

   function validateform1(Request $request){
      $request -> validate([      
         'username' => 'required',
         'password' => 'required|min:8',
     ]);

     $uname = $request-> input('username');
     $pass = $request-> input('password');

     $user = DB::table('user')->where('username',$uname)->
     where('password', $pass)->pluck('username');        

     if($user){
           $var = str_replace("[", "", $user);
           $var2 = str_replace("]", "", $var);
           $var3 = str_replace('"', "", $var2);

         return redirect('dashboard/'.$var3);
         
     } else
         return back()->with('fail', 'Invalid Login Details');

   }

       function validateform2(Request $request){
         $request -> validate([      
            'username' => 'required',
            'password' => 'required|min:8',
        ]);

        $uname = $request-> input('username');
        $pass = $request-> input('password');

        $admin = DB::table('admin')->where('username',$uname)->
        where('password', $pass)->pluck('username'); 

        //extract username = ["admin"]
        if($admin){
              $var = str_replace("[", "", $admin);
              $var2 = str_replace("]", "", $var);
              $var3 = str_replace('"', "", $var2);

            
            return redirect('admin-dashboard/'.$var3); 
            
        } else
            return back()->with('fail', 'Invalid Login Details');

       }

       function validateform3(Request $request){
         $request -> validate([      
            'app_date' => 'required|after:today',
            'time' => 'required',
            'app_purpose' => 'required',
        ]);

        $uname = $request -> input ('hidden');

        $user = DB::table('user')->where('username',$uname)->pluck('user_id');

                $var = str_replace("[", "", $user);
                $var2 = str_replace("]", "", $var);
                $var3 = str_replace('"', "", $var2);

                $date = $request-> input('app_date');
                $time =  $request-> input('time');

                //query for appointment input

              $data =DB::table('appointment')->where('date',$date)->where('time', $time)->where('status',0)->first();
            
                    if($data){
                        return back()->with('fail', 'The slot is unavailable or already taken');
                    } 
                    else{
                        $query = DB::table('appointment')->insert([
                            'user_id'=> $var3,
                            'purpose'=> $request-> input('app_purpose'),
                            'date'=> $date,
                            'time'=> $time,
                        ]);
            
                            return back()->with('success', 'Appointment Submmitted');
                    } 
       }

     public function regvalidateform(Request $request) {
      print_r($request->all());
      $this->validate($request,[
         'f_name'=>'required',
         'l_name'=>'required',
         'email'=>'required|email|unique:user,email',
         'username'=>'required|unique:user,username', 
         'password'=>'required',
        
      ]);

      $query = DB::table('user')->insert([
         'fname'=> $request-> input('f_name'),
         'lname'=> $request-> input('l_name'),
         'username'=> $request-> input('username'),
         'email'=> $request-> input('email'),
         'password' =>  $request-> input('password'),
     ]);

     if ($query){
        return back()->with('success', 'Successfuly Added');
     }   else return back()->with('fail', 'Data not added');
   }


   function dashboard(){

      $time = array(
         'item'=>DB::table('time')->get()
     );

      return view('dashboard',$time);
   }

   function adminDashboard(){

      $uname = request()->segment(2);
      $data =DB::table('admin')->where('username',$uname)->pluck('admin_id');

      $var = str_replace("[", "", $data);
      $var2 = str_replace("]", "", $var);
      $var3 = str_replace('"', "", $var2);


      $item = array(
         'data' => DB::table('appointment')
         ->join('time', 'appointment.time', '=', 'time.id')
         ->join('user', 'appointment.user_id', '=', 'user.user_id')
         ->get()
         );

      return view('admin-dashboard', $item);
   }
   
   function appointments(){

      $uname = request()->segment(2);

      $data =DB::table('user')->where('username',$uname)->pluck('user_id');

      $var = str_replace("[", "", $data);
      $var2 = str_replace("]", "", $var);
      $var3 = str_replace('"', "", $var2);


      $item = array(
         'data' => DB::table('appointment')
         ->join('time', 'appointment.time', '=', 'time.id')
         ->where('appointment.user_id',$var3)
         ->get()
         );

      return view('appointment', $item);

      
   }


   function deleteAppointment(Request $request) {
      $app_id = $request->input('app_id');

      $query = DB::table('appointment')->where('app_id', $app_id)->delete();

      if($query) {
         return back()->with('success', 'Appointment cancelled.');
      }
   }

   function approveAppointment(Request $request) {
      $app_id = $request->input('app_id');

      $query = DB::table('appointment')->where('app_id', $app_id)->delete();

      if($query) {
         return back()->with('success', 'Appointment Done!.');
      }
   }
}
