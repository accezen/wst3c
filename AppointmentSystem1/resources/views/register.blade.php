<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dr. Quack | Registration</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <style>
            *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Poppins';
        }

        html, body{
            height: 100vh;
            place-items: center;
            background: #a6c0ca;
        }

        .banner{
            margin-top:1%;
            color:#ffff;
            font-size: 32px;
            letter-spacing: 2px;
        }

        .form-container{
            background-color:#ffffff;
            padding: 8%;
            border-radius: 0px 0px 5px 5px;
        }

        .title{
            line-height: 75px;
            background: #008080;
            padding-left: 20px;
            border-radius: 5px 5px 0 0;
            font-weight: bolder;
            color: white;
            font-size: 25px;
        }

        #submit{
            color: white;
            font-weight: bold;
            padding-left: 0;
            background: #008080;
            border: 1px solid #008080;
            cursor: pointer;
            padding: 10px 12px;
            border-radius: 5px;
        }

        #submit:hover{
            color: white;
            font-weight: bold;
            padding-left: 0;
            background: #02a2a2;
            border: 1px solid #02a2a2;
            cursor: pointer;
            padding: 10px 12px;
            border-radius: 5px;
        }


        .form-control:focus{
            border: 1px solid #008080;
            border-color: #008080;
        }

        input{
            margin-bottom: 1%;
        }

        .link{
            text-decoration: none;
            color: #008080;
            font-weight: bold;
        }

        .link:hover{
            color:#02a2a2;
            transition: all 0.4s ease;
        }

        .error{
            color: red;
        }
        </style>
    </head>

    <body>
        <div class="banner">
            <center><p>Register<p></center>
        </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <div class="title">
                            <span>Registration Form</span>
                        </div>

                       
                        
                        <div class="form-container">



                        @if (Session::get('fail'))
                        <div class="alert alert-danger mb-3" role="alert">                   
                        <span class="text-danger"> {{Session::get('fail')}}</span>                      
                        </div>                           
                        @endif

                        @if (Session::get('success'))
                        <div class="alert alert-success mb-3" role="alert">
                        <span class="text-success"> {{Session::get('success')}}</span>       
                        </div> 
                        @endif
                            <form class="m-0" action="{{('/register')}}" method="POST">
                                @csrf
                                <div class="form-group pt-2">
                                    <input type="text" class="form-control" name="f_name" placeholder="Firstname" value="{{old ('f_name')}}">
                                </div>

                                @if ($errors->has('f_name'))
                                <span class="text-danger">{{ $errors->first('f_name') }}</span>
                                @endif
                        

                                <div class="form-group pt-2">
                                    <input type="text" class="form-control"  name="l_name" placeholder="Lastname" value="{{old ('l_name') }}">
                                </div>

                                @if ($errors->has('l_name'))
                                <span class="text-danger">{{ $errors->first('l_name') }}</span>
                                @endif

                                <div class="form-group pt-2">
                                    <input type="email" class="form-control"  name="email" placeholder="Email" value="{{old ('email')}}">
                                </div>

                                @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif

                                <div class="form-group pt-2">
                                    <input type="text" class="form-control" name="username" placeholder="Username" value="{{old ('username')}}">
                                </div>

                                @if ($errors->has('username'))
                                <span class="text-danger">{{ $errors->first('username') }}</span>
                                @endif

                                <div class="form-group pt-2">
                                    <input type="password" class="form-control" name="password" id="new_password" placeholder="Enter New password" value="{{old ('password')}}">
                                </div>

                                @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif

                                <div class="d-grid gap-2 col-12 my-3">
                                    <input type = "submit" id="submit" value="REGISTER">   
                                </div>

                                <div class="signup-link">
                                    Have an account? <a class = "link" href = "{{ ('/login') }}">Login</a>.
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>