<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dr. Quack | LOGIN</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel = "stylesheet" href = "login.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
        .error{
            color: red;
        }
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Poppins';
        }

        html, body{
            display: grid;
            height: 100%;
            place-items: center;
            background:  #a6c0ca;
        }


        .banner{
            margin-top: 10%;
            color:#ffff;
            font-size: 40px;
            letter-spacing: 2px;
        }

        .wrapper{
            width: 400px;
            background: white;
            border-radius: 5px;
            margin-bottom: 20%;
        }

        .wrapper .title{
            line-height: 80px;
            background: #008080;;
            padding-left: 20px;
            border-radius: 5px 5px 0 0;
            font-weight: bold;
            color: white;
            font-size: 25px;
        }

        .wrapper form{
            padding: 30px 20px 25px 20px;
        }

        .wrapper form .row{
            height: 45px;
            margin-bottom: 15px;
            position: relative;
        }

        .wrapper form .row input{
            height: 100%;
            width: 100%;
            padding-left: 60px;
            outline: none;
            border-radius: 5px;
            border: 1px solid lightgray;
            font-size: 16px;
        }

        .wrapper form .row i{
            position: absolute;
            width: 47px;
            height: 100%;
            color: white;
            font-size: 18px;
            background: #008080;
            border: 1px solid  #008080;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 5px 0 0 5px;
        }

        .wrapper form .button input{
            color: white;
            font-weight: bold;
            padding-left: 0;
            background:  #008080;
            border: 1px solid  #008080;
            cursor: pointer;
        }

        #submit:hover{
            color: white;
            font-weight: bold;
            padding-left: 0;
            background:  #02a2a2;
            border: 1px solid #02a2a2;
            cursor: pointer;
            transition: all 0.4s ease;
        }
        .wrapper form .pass{
            margin: -8px 0 15px 0;
        }

        .wrapper form .pass a{
            color: #C9AE5D;
            text-decoration: none;
        }

        .wrapper form .pass a:hover{
            text-decoration: underline;
        }

        .link{
            text-decoration: none;
            color: #008080;
            font-weight: bold;
        }

        .link:hover{
            color:#02a2a2;
            text-decoration: underline;
            transition: all 0.4s ease;
        }

    </style>
    
    </head>
    <body>
        <div class="banner">
            <p>Dr. Quack Appointment System<p>
        </div>

        <div class="wrapper">
            <div class="title">
                <span>Patient Login</span>
            </div>

            <form action="{{ ('/login') }}" method="POST">
                @csrf
                <div class="mx-2 row">
                    <i class = "fa fa-user"></i>
                    <input type="text" name="username" placeholder="Username" value="{{old ('username') }}">
                </div>

                <div class="mb-3">
                @if ($errors->has('username'))
                <span class="text-danger">{{ $errors->first('username') }}</span>
                @endif
                </div>
            
                <div class="mx-2 row">
                    <i class = "fa fa-lock"></i>
                    <input type = "password" placeholder = "Password" name = "password" value="{{old ('password') }}">
                </div>

                <div class="mb-3">
                @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
                </div>

                <div class="mx-2 row button">
                    <input type = "submit" id="submit" value="LOGIN">
                </div>

                <div class="signup-link">
                    Don't have an account? <a class = "link" href = "{{ ('/register') }}">Register</a>.<br>
                    I'm a <a class = "link" href = "{{ ('/admin') }}">Admin</a>.
                </div>
            </form>
        </div>
    </body>
</html>