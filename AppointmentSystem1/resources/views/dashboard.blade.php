<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <style>
        .input{
            padding: 5px;
        }

        .btn{
            width: 100%;
        }

        a{
            text-decoration: none;
        }

        .body{
            margin-top: 1%;
            margin-left: 5%;
            margin-right: 5%;
        }

        .welcome{
            float: right;
        }

        .fill{
            margin-left: 10%;
            margin-right: 50%;
        }

        .action{
            width: 50%;
        }

        .container-fluid {
             padding: 0;
        }
        
        .banner-header {
        background-color: #008080;
        color: white;
        padding: 1.3%;
        }
        
        #banner-header {
        font-size: 24pt;
        margin-left: .5%;
        }

        a {
            color: white;
        }

        a:hover {
            color: #c4c4c4;
        }

        .submit:hover{
            color: white;
            padding-left: 0;
            background:  #02a2a2;
            border: 1px solid #02a2a2;
            cursor: pointer;
            transition: all 0.4s ease;
        }

        .submit{
            color: white;
            padding-left: 0;
            background:  #008080;
            border: 1px solid #008080;
            cursor: pointer;
            transition: all 0.4s ease;
        }

    </style>

    <body>

    <div class="container-fluid mt-0">
        <div class="banner-header">
            <div class="banner-wrapper">
            <span id="banner-header">Dr. Quack Medical Group</span>
            <div class="nav d-flex flex-row flex-end mt-3 justify-content-end ">
            <div class="p-2"><a href = "#">HOME</a></div>
                <?php $user = request()->segment(2); ?>
                <div class="p-2"><a href = "{{ ('/appointments/'.$user) }}">APPOINTMENTS</a></div>
                <div class="p-2"><a href = "{{ ('/login') }}"  onclick="return confirm('Do you want to logout?');">LOGOUT</a></div>
            </div>
            </div>
        </div> 
    </div>

    <div class="body">
            <div class="row pt-3">
                <div class="col-sm-12">
                    <h1>SET APPOINTMENT</h1>
                    <h5>Welcome!</h5>
                </div>
            </div>
        </div>

        @if (Session::get('fail'))
            <div class="alert alert-danger mb-3" role="alert">                   
                <span class="text-danger"> {{Session::get('fail')}}</span>                      
            </div>                           
        @endif

        @if (Session::get('success'))
            <div class="alert alert-success mb-3" role="alert">
                <span class="text-success"> {{Session::get('success')}}</span>       
            </div> 
        @endif

        <form action = "{{ ('/home') }}" method = "POST">
            @csrf
            <div class="row fill mt-5">
                
                <input type="hidden" class="form-control" name = "hidden" value="<?php echo request()->segment(2); ?>">
                    
                <div class="col-sm-12">
                    <label>Date</label>
                    <input type="date" class="form-control" name = "app_date" value="{{old ('app_date')}}">
                    @if ($errors->has('app_date'))
                    <span class="text-danger">{{ $errors->first('app_date') }}</span>
                    @endif
                </div>

                <div class="mb-3">
                
                </div>

                <div class="col-sm-12">
                    <label>Time</label>
                    <select class = "form-select" name = "time">
                       <option selected disabled>Select Time</option>
                       @foreach ($item as $time)
                        <option value = "{{$time->id}}">{{$time->time}}</option>
                       @endforeach
                    </select>
                    @if ($errors->has('time'))
                    <span class="text-danger">{{ $errors->first('time') }}</span>
                    @endif
                </div>

                <div class="col-sm-12">
                    <label>Purpose</label>
                    <textarea class="form-control" name = "app_purpose"></textarea>
                    @if ($errors->has('app_purpose'))
                    <span class="text-danger">{{ $errors->first('app_purpose') }}</span>
                    @endif
                </div>

                <div class="col-sm-12">
                    <input type = "submit" class = "btn submit mt-3" name = "submit"  >
                </div>
            </div>
        </form>
    </body>
</html>