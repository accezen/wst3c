<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin Dashboard</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">​
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>

    </head>
    <style>
        .input{
            padding: 5px;
        }

        .btn{
            width: 100%;
        }

        a{
            text-decoration: none;
        }

        .body{
            margin-top: 1%;
            margin-left: 5%;
            margin-right: 5%;
        }

        .welcome{
            float: right;
        }

        .fill{
            margin-left: 10%;
            margin-right: 50%;
        }

        .action{
            width: 50%;
        }

        .container-fluid {
             padding: 0;
             margin-top: -1.6%;
        }
        
        .banner-header {
        background-color: #008080;
        color: white;
        padding: 1.3%;
        }
        
        #banner-header {
        font-size: 24pt;
        margin-left: .5%;
        }

        a {
            color: white;
        }

        a:hover {
            color: #c4c4c4;
        }
        
    </style>

    <body>

        <div class="container-fluid">
            <div class="banner-header">
                <div class="banner-wrapper">
                <span id="banner-header">Dr. Quack Medical Group</span>
                <div class="nav d-flex flex-row flex-end mt-3 justify-content-end ">
                <?php $user = request()->segment(2); ?>
                <div class="p-2"><a href = "{{ ('/admin-dashboard/'.$user) }}">HOME</a></div>
                    <div class="p-2"><a href = "{{ ('/admin') }}"  onclick="return confirm('Do you want to logout?');">LOGOUT</a></div>
                </div>
                </div>
            </div> 
        </div>

        <div class="body">
            <div class="row">
                <div class="col-sm-12 mb-4">
                    <h1>MY APPOINTMENTS</h1>
                    <h3>Hello Admin!</h3>
                </div>
                <br>
                @if(Session::get('success'))
                <div class = "alert alert-success p-2">
                    <p>{{Session::get('success')}}</p>
                </div>
            @endif
                <div class="col-sm-6">
                @foreach ($data as $data)
                    <div class="card border-success mb-3" style="max-width: 50rem;">
                        <div class="card-header bg-dark text-light">Date: {{$data->date}}</div>
                        <div class="card-body text-dark">
                            <h4 class="card-title text-info">{{$data->fname}} {{$data->lname}}</h5>
                            <h6 class="card-title">Time: {{$data->time}}</h5>
                            <h6><p class="card-title">Purpose</p></h4>
                            <p class="card-text">{{$data->purpose}}</p>
                            <div class="d-flex flex-row-reverse">
                                    <div class="col-sm-4">
                                    <form action="/approve" method="POST">
                                        @csrf
                                        <input type='hidden' value="{{$data->app_id}}" name="app_id">
                                        <button class="btn btn-primary" >Appointment Done</button>
                                    </form>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>

        
    </body>
</html>