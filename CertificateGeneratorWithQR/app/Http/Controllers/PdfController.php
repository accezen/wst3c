<?php

namespace App\Http\Controllers;

use Nette\Utils\Image;
use App\Models\Training;
use App\Models\Certificate;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Generator;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class PdfController extends Controller
{
    protected $fpdf;
 
    public function __construct()
    {
        $this->fpdf = new Fpdf('L','mm',array(100,100));
    }

    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'description' => 'required',
            'date' => 'required',
            'organizer' => 'required',
            'position' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png',
            'template' => 'required'
        ]);

        // model
        $certificate = new Certificate();

        $name = $request->get('name');
        $description = $request->get('description');
        $date = $request->get('date');
        $organizer = $request->get('organizer');
        $position = $request->get('position');
        $template = $request->get('template');

        // validation
        if($validatedData->fails()){
            return redirect()->back()->withErrors($validatedData)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/image/logo/', $filename);
                
                $certificate->training_id = $request->get('training_id');
                $certificate->certificate_id = Str::random(15);
                $certificate->name = $name;
                $certificate->description = $description;
                $certificate->date = $date;
                $certificate->organizer = $organizer;
                $certificate->position = $position;
                $certificate->image = $filename;
                $certificate->template = $template;
                $certificate->save();
            }
            return redirect()->back()->with('success', 'Certificate Generated');
        }
    }

    public function view($id)
    {
        $fetch = Certificate::find($id);
        if(isset($fetch->id)){
            if($fetch->template == "Template 1"){
            $name= $fetch->name;
            $content= $fetch->description;
            $logo = 'assets/image/template_image/img2.png';
            $organizer = $fetch->organizer;
            $position = $fetch->position;
            $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";

            $this->fpdf->SetFont('Arial', 'B', 60);
            $this->fpdf->AddPage("L", ['500', '530']);
            $this->fpdf->SetMargins(40,80,200,80);
            $this->fpdf->Image($logo,0,0,0,0);
            $this->fpdf->Image('assets/image/logo/'.$fetch->image,370,140,100);
            
           $this->fpdf->Ln(170); 
            $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
            $this->fpdf->Ln(20);
            $this->fpdf->SetFont('Arial','B',20);
            $this->fpdf->MultiCell(0, 15,$content,100,'C',0);
            $this->fpdf->Ln(25);
            $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
            $this->fpdf->SetFont('Arial','B',15);
            $this->fpdf->Ln(3);
            $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
            $this->fpdf->Ln(10);
            $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
            $this->fpdf->Ln(15);
            $this->fpdf->Image("$url",40,300,0,100,'PNG');
            $this->fpdf->Ln(20); 
            
        }

        else if($fetch->template == "Template 2"){
            $name= $fetch->name;
            $content= $fetch->description;
            $logo = 'assets/image/template_image/img3.png';
            $organizer = $fetch->organizer;
            $position = $fetch->position;
            $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";

            $this->fpdf->SetTextColor(255, 255, 255);

            $this->fpdf->SetFont('Arial', 'B', 60);
            $this->fpdf->AddPage("L", ['500', '530']);
            $this->fpdf->SetMargins(80,80,80,80);
            $this->fpdf->Image($logo,0,0,0,0);

            $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
            
            $this->fpdf->Ln(170);
            $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
            $this->fpdf->Ln(20);
            $this->fpdf->SetFont('Arial','B',20);
            $this->fpdf->MultiCell(0, 15,$content,100,'C',0);
            $this->fpdf->Ln(25);
            $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
            $this->fpdf->SetFont('Arial','B',15);
            $this->fpdf->Ln(3);
            $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
            $this->fpdf->Ln(10);

            $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
            $this->fpdf->Image("$url",40,300,0,50,'PNG');
            $this->fpdf->Ln(20); 

            }

            else{

                $name= $fetch->name;
                $content= $fetch->description;
                $logo = 'assets/image/template_image/img5.png';
                $organizer = $fetch->organizer;
                $position = $fetch->position;
                $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";

                $this->fpdf->SetFont('Arial', 'B', 60);
                $this->fpdf->AddPage("L", ['500', '530']);
                $this->fpdf->SetMargins(80,80,80,80);
                $this->fpdf->Image($logo,0,0,0,0);

                $this->fpdf->Image('assets/image/logo/'.$fetch->image,44,48,35);
                
                $this->fpdf->Ln(170);
                $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                $this->fpdf->Ln(20);
                $this->fpdf->SetFont('Arial','B',20);
                $this->fpdf->MultiCell(0, 15,$content,100,'C',0);
                $this->fpdf->Ln(25);
                $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                $this->fpdf->SetFont('Arial','B',15);
                $this->fpdf->Ln(3);
                $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                $this->fpdf->Ln(10);
                $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                $this->fpdf->Image("$url",40,300,0,50,'PNG');
                $this->fpdf->Ln(20); 
            }
        }
        return view($this->fpdf->Output());
            
        exit;
    }

    public function index() 
    {
        $this->fpdf->Output();
        exit;
    }
}
