<?php

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customer/{id}/{name}/{address}', [OrderController::class,'customer']);
Route::get('/item/{item}/{name}/{price}', [OrderController::class,'item']);
Route::get('/orders/{id}/{name}/{ordNo}/{date}', [OrderController::class,'orders']);
Route::get('/details/{transNo}/{ordNo}/{itemID}/{name}/{price}/{qty}', [OrderController::class,'details']);

