<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Item</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<h1 class="mb-5">Item</h1>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Item No </label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value=<?php echo $item;?> readonly> 
               </div>
          </div>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Name</label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value=<?php echo $name;?> readonly> 
               </div>
          </div>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Price</label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value=<?php echo $price;?> readonly>
               </div>
          </div>
</body>
</html>