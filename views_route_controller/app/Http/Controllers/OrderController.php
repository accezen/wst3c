<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function customer($id,$name,$address){
        return view('customer')
        ->with('id', $id)
        ->with('name', $name)
        ->with('address', $address);
    }

    public function item($item,$name,$price){
        return view('item')
        ->with('item', $item)
        ->with('name', $name)
        ->with('price', $price);
    }

    public function orders($id,$name,$ordNo,$date){
        return view('order')
        ->with('id', $id)
        ->with('name', $name)
        ->with('ordNo', $ordNo)
        ->with('date',$date);
    }


    public function details($transNo,$ordNo,$itemID,$name,$price,$qty){
        return view('details')
        ->with('transNo', $transNo)
        ->with('ordNo', $ordNo)
        ->with('itemID', $itemID)
        ->with('name',$name)
        ->with('price',$price)
        ->with('qty',$qty);
    }
}
