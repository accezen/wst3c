<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
</head>


<body>

<h2 class="pt-3">List of Products</h2>
@foreach ($products as $p)
<div class="products_container m-3">
    <p class="name pt-0"><b>Name: </b> {{$p->name}}</p>
    <p class="price"> <b>Price: </b>{{$p->price}}</p>
    <p class="qty"><b>Items left: </b> {{$p->stocks}}</p>
    <br>
    <br>
</div>
@endforeach

<h2>List of Users</h2>
@foreach ($users as $u)
<div class="products_container m-3">
    <p class="name pt-0"><b>Username: </b> {{$u->username}}</p>
    <p class="name pt-0"><b>Email: </b> {{$u->email}}</p>
    <br>
</div>

@endforeach

    
</body>
</html>