<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\GetController;
use App\Http\Controllers\ValidationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login/admin',[GetController::class,
    'showAdminLogin'
]);

Route::post('/login/admin', [ValidationController::class,
    'loginValidate'
]);

Route::get('/admin/dashboard',[GetController::class,
    'adminHome'
]);
