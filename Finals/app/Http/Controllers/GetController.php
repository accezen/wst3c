<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetController extends Controller
{
    function showAdminLogin(){
        return view ('admin-login');
    }

    function adminHome(){
        
        $data = array(
            'products'=>DB::table('products')->get(),
            'users'=>DB::table('users')->get(),
            
        );
        return view('admin-dashboard',$data);
    } 
}
