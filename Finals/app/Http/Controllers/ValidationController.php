<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ValidationController extends Controller
{
    function loginValidate(Request $request){
    
        $email = $request-> input('email');
        $password = $request-> input('password');

        $user = DB::table('admin')->where('email',$email)->where('password', $password);


    if($user->count()== 1){
        return redirect('admin/dashboard');
    } 
    else{
        return back()->with('error', 'Invalid user credentials');
    }       
}
}
