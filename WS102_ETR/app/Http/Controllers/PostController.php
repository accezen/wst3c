<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    function editBanner (Request $request){

        $banner_img = $request -> file('banner_img');
        $heading = $request->input('banner-heading');
        $sub_heading = $request->input('banner-sub-heading');
        $time = $request->input('time');
        $color = $request->input('color');

        $imageName = time().'.'.$request->banner_img->extension();  
        $path = $request->banner_img->move(public_path('images'), $imageName);

        
        $query = DB::table ('home_page')->update([
            'banner_img' => $imageName,
            'heading' => $heading,
            'sub_heading' => $sub_heading,
            'time' => $time,
            'color' => $color,
        ]);

        if($query){
            return back()->with('message','Data added Successfully');
         }   
        else{
            return back()->with('error', 'Data not added');
         }
    }

    function addBestSellers (Request $request){

        $bs_image = $request -> file('bs_img');
        $bs_desc = $request->input('bs_desc');
        $bs_price = $request->input('bs_price');

        $imageName = time().'.'.$request->bs_img->extension();  
        $path = $request->bs_img->move(public_path('images'), $imageName);

        
        $query = DB::table ('best_sellers')->insert([
            'bs_image' => $imageName,
            'bs_desc' => $bs_desc,
            'bs_price' => $bs_price
        ]);

        if($query){
            return back()->with('message','Data added Successfully');
         }   
        else{
            return back()->with('error', 'Data not added');
         }
    }

    function addSulitMeals (Request $request){

        $sm_image = $request -> file('sm_img');
        $sm_desc = $request->input('sm_desc');
        $sm_price = $request->input('sm_price');

        $imageName = time().'.'.$request->sm_img->extension();  
        $path = $request->sm_img->move(public_path('images'), $imageName);

        
        $query = DB::table ('sulit_meals')->insert([
            'sm_image' => $imageName,
            'sm_desc' => $sm_desc,
            'sm_price' => $sm_price
        ]);

        if($query){
            return back()->with('message','Data added Successfully');
         }   
        else{
            return back()->with('error', 'Data not added');
         }
    }

    function addOrg (Request $request){

        $org_img = $request -> file('org_img');
        $name = $request->input('name');
        $position = $request->input('position');

        $imageName = time().'.'.$request->org_img->extension();  
        $path = $request->org_img->move(public_path('images'), $imageName);

        
        $query = DB::table ('org')->insert([
            'org_img' => $imageName,
            'name' => $name,
            'position' => $position
        ]);

        if($query){
            return back()->with('message','Record Added Successfully!');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }
    }

    function addBarkadaBundle (Request $request){

        $bb_image = $request -> file('bb_img');
        $bb_desc = $request->input('bb_desc');
        $bb_price = $request->input('bb_price');

        $imageName = time().'.'.$request->bb_img->extension();  
        $path = $request->bb_img->move(public_path('images'), $imageName);

        
        $query = DB::table ('barkada_bundle')->insert([
            'bb_image' => $imageName,
            'bb_desc' => $bb_desc,
            'bb_price' => $bb_price
        ]);

        if($query){
            return back()->with('message','Data added Successfully');
         }   
        else{
            return back()->with('error', 'Data not added');
         }
    }

    function addFaqs (Request $request){

    
        $question = $request->input('question');
        $answer = $request->input('answer');

        
        $query = DB::table ('faqs')->insert([
            'question' => $question,
            'answer' => $answer
           
        ]);

        if($query){
            return back()->with('message','Data added Successfully');
         }   
        else{
            return back()->with('error', 'Data not added');
         }
    }


    function deleteBSItem (Request $request){

        $bs_id = $request -> input('bs_id');
        
        $query = DB::table('best_sellers')->where('bs_id', $bs_id)->delete();

        if($query){
            return back()->with('error','Record Deleted');
         }   
        else{
            return back()->with('error', 'Something went wrong');
         }
    }

    function deleteSMItem (Request $request){

        $sm_id = $request -> input('sm_id');
        
        $query = DB::table('sulit_meals')->where('sm_id', $sm_id)->delete();

        if($query){
            return back()->with('error','Record Deleted');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }
    }

    function deleteBBItem (Request $request){

        $bb_id = $request -> input('bb_id');
        
        $query = DB::table('barkada_bundle')->where('bb_id', $bb_id)->delete();

        if($query){
            return back()->with('error','Record Deleted');
         }   
        else{
            return back()->with('error', 'Something went wrong');
         }
    }

    function deleteOrgItem (Request $request){

        $org_id = $request -> input('org_id');
        
        $query = DB::table('org')->where('org_id', $org_id)->delete();

        if($query){
            return back()->with('error','Record Deleted');
         }   
        else{
            return back()->with('error', 'Something went wrong');
         }
    }

    function deleteFaqsItem (Request $request){

        $f_id = $request -> input('f_id');
        
        $query = DB::table('faqs')->where('f_id', $f_id)->delete();

        if($query){
            return back()->with('error','Record Deleted');
         }   
        else{
            return back()->with('error', 'Something went wrong');
         }
    }

    function updateStory (Request $request){

        
        $story = $request->input('story');

        
        $query = DB::table ('story')->update([
            'story' => $story,
        ]);

        if($query){
            return back()->with('message','Data Updated Successfully.');
         }   
        else{
            return back()->with('error', 'No change has detected.');
         }
    }

    function editFB (Request $request){

        $link = $request->input('link');

        
        $query = DB::table ('social')->update([
            'fb_link' => $link,
        ]);

        if($query){
            return back()->with('message','Link Updated Successfully.');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }
    }

    function editGmail (Request $request){

        $link = $request->input('link');

        
        $query = DB::table ('social')->update([
            'gmail_link' => $link,
        ]);

        if($query){
            return back()->with('message','Link Updated Successfully.');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }
    }


    function editInstagram (Request $request){

        $link = $request->input('link');

        
        $query = DB::table ('social')->update([
            'instagram_link' => $link,
        ]);

        if($query){
            return back()->with('message','Link Updated Successfully.');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }
    }

    function editLogo (Request $request){

        $logo_img = $request -> file('logo_img');
        
        $imageName = time().'.'.$request->logo_img->extension();  
        $path = $request->logo_img->move(public_path('images'), $imageName);

        
        $query = DB::table ('logo')->update([
            'logo_img' => $imageName,
            
        ]);

        if($query){
            return back()->with('message','Logo Updated Successfully');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }
    }

    function enlistBSItem (Request $request){

        $bs_id = $request -> input('bs_id');
        $list = $request -> input('list');

        if ($list==1){

            $enlist = 0;
            $stat = "Are you sure you want to remove the item from the list?";
            $color = "#D98B19";

            $query = DB::table('best_sellers')->where('bs_id', $bs_id)->update([
                'listing' => $enlist,
                'stat'=>$stat,
                'color'=>$color,
            ]);
    
            if($query){
                return back()->with('message','Item added back to list');
             }   
            else{
                return back()->with('error', 'Something went wrong.');
             }
        } else{

            $enlist = 1;
            $stat = "Are you sure you want to add the item back to list?";
            $color = "gray";
        
        $query = DB::table('best_sellers')->where('bs_id', $bs_id)->update([
            'listing' => $enlist,
            'stat'=>$stat,
            'color'=>$color,
        ]);

        if($query){
            return back()->with('error','Item unlisted');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }

        }
        
    }


    function enlistSMItem (Request $request){

        $sm_id = $request -> input('sm_id');
        $list = $request -> input('list');

        if ($list==1){

            $enlist = 0;
            $stat = "Are you sure you want to remove the item from the list?";
            $color = "#D98B19";

            $query = DB::table('sulit_meals')->where('sm_id', $sm_id)->update([
                'listing' => $enlist,
                'stat'=>$stat,
                'color'=>$color,
            ]);
    
            if($query){
                return back()->with('message','Item added back to list');
             }   
            else{
                return back()->with('error', 'Something went wrong.');
             }
        } else{

            $enlist = 1;
            $stat = "Are you sure you want to add the item back to list?";
            $color = "gray";
        
        $query = DB::table('sulit_meals')->where('sm_id', $sm_id)->update([
            'listing' => $enlist,
            'stat'=>$stat,
            'color'=>$color,
        ]);

        if($query){
            return back()->with('error','Item unlisted');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }

        }
        
    }


    function enlistBBItem (Request $request){

        $bb_id = $request -> input('bb_id');
        $list = $request -> input('list');

        if ($list==1){

            $enlist = 0;
            $stat = "Are you sure you want to remove the item from the list?";
            $color = "#D98B19";

            $query = DB::table('barkada_bundle')->where('bb_id', $bb_id)->update([
                'listing' => $enlist,
                'stat'=>$stat,
                'color'=>$color,
            ]);
    
            if($query){
                return back()->with('message','Item added back to list');
             }   
            else{
                return back()->with('error', 'Something went wrong.');
             }
        } else{

            $enlist = 1;
            $stat = "Are you sure you want to add the item back to list?";
            $color = "gray";
        
        $query = DB::table('barkada_bundle')->where('bb_id', $bb_id)->update([
            'listing' => $enlist,
            'stat'=>$stat,
            'color'=>$color,
        ]);

        if($query){
            return back()->with('error','Item unlisted');
         }   
        else{
            return back()->with('error', 'Something went wrong.');
         }

        }
        
    }
}
