<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ValidationController extends Controller
{

    function loginValidate(Request $request){
    
            $request -> validate([      
            'email' => 'required',
            'password' => 'required|min:8',
        ]);

            $email = $request-> input('email');
            $password = $request-> input('password');

            $user = DB::table('user')->where('user_email',$email)->where('user_password', $password);


        if($user->count()== 1){
            return redirect('admin/dashboard');
        } 
        else{
            return back()->with('error', 'Invalid user credentials');
        }       
    }

    function updatePassword(Request $request){
    
        $request -> validate([      
           'c_pass' => 'required',
           'password' => 'required|min:8|same:c_pass',
       ]);

        $c_pass = $request-> input('c_pass');
        $password = $request-> input('password');

        $query = DB::table ('user')->update([
            'user_password' => $password,
        ]);

        if($query){
            return back()->with('message','Password has changed successfully.');
         }   
        else{
            return back()->with('error', 'Password not match.');
         }
    }      
}
