<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetController extends Controller
{
    function showAdminLogin(){
        return view ('admin.admin-login');
    }
    
    function adminHome(){
        
            $data = array(
                'home_page'=>DB::table('home_page')->get(),
                'logo'=>DB::table('logo')->get()
            );
            return view('admin.admin-dashboard', $data);
        } 


    public function adminMenu(){

        $data = array(
            'best_sellers'=>DB::table('best_sellers')->orderBy('listing', 'asc')->orderBy('bs_desc', 'asc')->get(),
            'sulit_meals'=>DB::table('sulit_meals')->orderBy('listing', 'asc')->get(),
            'barkada_bundle'=>DB::table('barkada_bundle')->orderBy('listing', 'asc')->get(),
            'logo'=>DB::table('logo')->get()
        );
        
        return view('admin.admin-menu', $data);
    }

    function adminFaqs(){

        $data = array(
            'faqs'=>DB::table('faqs')->get(),
            'logo'=>DB::table('logo')->get()
        );

        return view('admin.admin-faqs', $data);
    }

    function adminAbout(){

        $data = array(
            'story'=>DB::table('story')->get(),
            'org' =>DB::table('org')->get(),
            'logo'=>DB::table('logo')->get()
            
        );
        
        return view('admin.admin-about', $data);
    }

    function adminSettings(){

        $data = array(
            'social'=>DB::table('social')->get(),
            'logo'=>DB::table('logo')->get()
        );
        
        return view('admin.admin-settings', $data);
    }

    function Home(){

        $data = array(
            'home_page'=>DB::table('home_page')->get(),
            'logo'=>DB::table('logo')->get(),
            'best_sellers'=>DB::table('best_sellers')->get(),
            'social'=>DB::table('social')->get(),
        );

        return view('client.home', $data);
     
    } 
        


    function Menu(){

        

        $data = array(
            'logo'=>DB::table('logo')->get(),
            'best_sellers'=>DB::table('best_sellers')->where('listing',0)->get(),
            'sulit_meals'=>DB::table('sulit_meals')->where('listing',0)->get(),
            'barkada_bundle'=>DB::table('barkada_bundle')->where('listing',0)->get(),
            'social'=>DB::table('social')->get(),
        );

        return view('client.menu', $data);
    }

    function About(){

        $data = array(
            'logo'=>DB::table('logo')->get(),
            'story'=>DB::table('story')->get(),
            'org'=>DB::table('org')->get(),
            'social'=>DB::table('social')->get(),
        );

        return view('client.about', $data);
    }

    function Faqs(){

        $data = array(
            'logo'=>DB::table('logo')->get(),
            'social'=>DB::table('social')->get(),
            'faqs'=>DB::table('faqs')->get()
        );

        return view('client.faqs', $data);
    }
}
