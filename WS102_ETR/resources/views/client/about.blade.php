<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <style>

    html {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Montserrat', sans-serif;
        scroll-behavior: smooth;
    }

    .landing-banner{
        margin: 0;
        padding: 0;
        height: 100vh;
        background-size: cover;
    }

    .banner-img{
        margin: 0;
        padding: 0;
        height: 100vh;
        background-size: cover;
    }

    .main-container{
        height: auto;
    }

    .nav {
        position: fixed;
        width: 100%;
        background: rgba(0, 0, 0, .4);
        backdrop-filter: blur(30px);
        padding: 10px;
        z-index: 2;
    }

    .logo {
        height: 60px;
    }

    .link {
        text-decoration: none;
        color: white;
        font-weight: 500;
        margin-right: 30px;
        padding-bottom: 5px;
    }

    .link:hover {
        color: #D98B19;
        border-bottom: 3px solid #D98B19;
    }

    .active {
        border-bottom: 3px solid #D98B19;
    }

    .banner-bg{
        height: 40vh;
        background-size: cover;
    }

    .banner-wrapper{
        margin-top: 10%;
        font-size: 55px;
        font-weight:700;
        color: white;
        letter-spacing: 5px;
    }

    .banner-title{
        font-size: 60px;
        color: white;
        letter-spacing: 8px;
    }

    .banner-sub{
        font-size: 24px;
        color: white;
        width: 500px;
    }

    .btn-container{
        margin-top: 5%;
    }

    .btn-menu{
        color:#D98B19;
        border:2px solid #D98B19;
        border-radius: 30px;
        margin-left: 50%;
    }

    .btn-menu:hover{
        color:#f2b31f;
        border:2px solid #f2b31f;
        border-radius: 30px;
        margin-left: 50%;
    }

    .bs-container{
        background-color: rgba(126, 123, 123, .18);
        height:auto;
        padding-bottom: 8%;
    }

    .bb-container{
        background-color: rgba(126, 123, 123, .18);
        height:auto;
    }

    .sm-container{
        background-color: white;
        height:auto;
        padding-bottom: 1%;
    }

    .container-heading-wrapper{
        padding-top: 3%;
        padding-left: 3%;
        font-size: 50px;
        font-weight: bold;
    }

    .one{
        color: #D98B19;
    }

    .two{
        color: #50504F;
    }

    .bxs-star{
        color: #D98B19;
    }

    .empty{
        font-size: 18px;
    }

    .sub_empty{
        font-size: 16px;
        color:#433E3B;
    }

    .empty_illustration{
        width: 300px;
    }

    .space{
        margin-top: 5%;
    }

    .bx{
        font-size: 24px;
    }

    .icons{
        color: #BFBBBB;
    }

    .bx:hover{
        color: #ffbf00;
    }

    .associations{
        color: #BFBBBB;
    }

    .footer{
        height: 150px;
        background-color: #50504F;
        padding-bottom: 0;
        padding-top: 5%;
    }

    .content-img {
        display: flex;
        width: 200px;
        height: 120px;
        float: left;
        margin: 3%;
        padding: 3px;
        border: solid #CE8719;
        border-radius: 200%;
        justify-content: center;
    }

    .card{
        margin: 2%;
        border-radius: 10px; 
        z-index: 0;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
    }

    .content-img img{
        max-width: 100%;
        height: auto;
        border-radius: 10px;
    }

    .card-img{
        border-radius: 50%;
        object-fit: cover;
        height: 180px;
        width: 180px;
        border: solid #D98B19;
    }

    .position{
        font-style: italic;
        color:#433E3B;
    }

    .name{
        color: #D98B19;
    }

    .branding{
        font-size: 24px;
        font-weight:bold;
        color: white;
    }

    .laman{
        padding-top: 1% !important;
    }

    </style>

</head>
    <body>
        <div class="d-flex justify-content-between align-items-center nav navbar">
            @foreach ($logo as $data)
            <div class="d-flex align-items-center p-1">
                    <span class="logo"><img src="{{url('/images/'. $data->logo_img) }}" class="logo" alt="Camp SaWings Logo" /></span>
                    <div class="branding">
                        <span>CAMP SA WINGS</span>
                    </div>       
            </div>
            @endforeach

            <div class="d-flex">
                <div>
                    <a class="link" href="{{('/home')}}">Home</a>
                    <a class="link" href="{{('/menu')}}">Menu</a>
                    <a class="link active" href="{{('/about')}}">About</a>
                    <a class="link" href="{{('/faqs')}}">FAQs</a>
                </div>
            </div>
        </div>


        <div class="banner-bg" style="background-image: url('/images/about-accent.png')">
            <div class=" banner-content d-flex flex-column align-items-start justify-content-center">
                <div class="banner-wrapper d-flex align-self-center">
                    <div>
                        <span>ABOUT</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container">
            <div class="bb-container">
                <div class="container-heading-wrapper">
                    <span class="container-heading one">STORY</span>
                </div>
                <div class="laman" style="text-align: justify;text-justify: inter-word; padding: 5%;">
                    @foreach ($story as $s)
                    <?php
                        $output = $s->story;
                    ?>
                    <p>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo $output;?>
                    </p>
                    @endforeach
                </div>
            </div>

            <div class="sm-container">
                <div class="container-heading-wrapper">
                    <span class="container-heading one">ORGANIZATION</span>
                </div>

                <div class="d-flex justify-content-center  px-4 flex-wrap py-3">
                    @foreach ($org as $data)
                    <div class="card" style="width: 15rem;">
                        
                        <div class="card-body d-flex justify-content-center">
                            <img class="card-img" src="{{ url ('/images/' . $data->org_img)}}">
                            
                        </div>
                        <div class="name d-flex justify-content-center ">
                            <center><p class="name m-0" style="font-size: 18px; font-weight: bold; ">{{$data->name}} </p></center>  
                        </div>

                        <div class="position d-flex justify-content-center ">
                            <center>
                                <p class=" position ">
                                    {{$data->position}} 
                                </p>
                            </center>
                        </div>  
                    </div>
                    @endforeach
                </div>
            </div>

        </div>

        <!-- Footer -->
        <div class="footer">
            <div class="d-flex justify-content-center">
                @foreach ($social as $link)
                <div class="icons">
                    <a href="{{$link->fb_link}}" style="color:#BFBBBB;" target="_blank"><i class='bx bxl-facebook-square'></i></a>
                </div>
                <div class="icons">
                    <a href="{{$link->instagram_link}}" style="color:#BFBBBB;" target="_blank"><i class='bx bxl-instagram'></i></a>
                </div>
                <div class="icons">
                    <a href="{{'mailto:'.$link->gmail_link}}" style="color:#BFBBBB;" target="_blank"><i class='bx bxl-gmail'></i></a>
                </div>
                @endforeach 
            </div>
            <div class="d-flex justify-content-center">
                <?php
                    $year = date('Y');
                ?>
                <div class="associations">
                    <span>@<?php echo $year ?>- Accezen. All Right Reserved. Designed and Developed by Jan Patrick Delacruz Urbano</span>
                </div>
            </div>
        </div>
        <!-- End of Footer -->


    </body>
</html>