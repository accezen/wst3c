<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <style>

    html {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Montserrat', sans-serif;
        scroll-behavior: smooth;
    }

    .landing-banner{
        margin: 0;
        padding: 0;
        height: 100vh;
        background-size: cover;
    }

    .banner-img{
        margin: 0;
        padding: 0;
        height: 100vh;
        background-size: cover;
    }

    .main-container{
        height: auto;
    }

    .nav {
        position: fixed;
        width: 100%;
        background: rgba(0, 0, 0, .4);
        backdrop-filter: blur(30px);
        padding: 10px;
        z-index: 2;
    }

    .logo {
        height: 60px;
    }

    .link {
        text-decoration: none;
        color: white;
        font-weight: 500;
        margin-right: 30px;
        padding-bottom: 5px;
    }

    .link:hover {
        color: #D98B19;
        border-bottom: 3px solid #D98B19;
    }

    .active {
        border-bottom: 3px solid #D98B19;
    }

    .banner-bg{
        height: 40vh;
        background-size: cover;
    }

    .banner-wrapper{
        margin-top: 10%;
        font-size: 55px;
        font-weight:700;
        color: white;
        letter-spacing: 5px;
    }

    .line {
        border-left: 5px solid #D98B19;
        padding-left: 30px;
        padding-top: 30px;
        padding-bottom: 30px;
    }

    .banner-title{
        font-size: 60px;
        color: white;
        letter-spacing: 8px;
    }

    .banner-sub{
        font-size: 24px;
        color: white;
        width: 500px;
    }

    .btn-container{
        margin-top: 5%;
    }

    .btn-menu{
        color:#D98B19;
        border:2px solid #D98B19;
        border-radius: 30px;
        margin-left: 50%;
    }

    .btn-menu:hover{
        color:#f2b31f;
        border:2px solid #f2b31f;
        border-radius: 30px;
        margin-left: 50%;
    }

    .bs-container{
        background-color: rgba(126, 123, 123, .18);
        height:auto;
        padding-bottom: 8%;
    }

    .bb-container{
        background-color: rgba(126, 123, 123, .18);
        height:auto;
        padding-bottom: 8%;
    }

    .sm-container{
        background-color: white;
        height:auto;
        padding-bottom: 8%;
    }

    .container-heading-wrapper{
        padding-top: 3%;
        padding-left: 3%;
        font-size: 50px;
        font-weight: bold;
    }

    .one{
        color: #D98B19;
    }

    .two{
        color: #50504F;
    }

    .card{
        
        margin: 2%;
        margin-top: 5%;
        border-radius: 25px; 
        z-index: 0;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
    }

    .card-footer:last-child {
        border-radius: 0px !important;
        background-color: transparent;
    }

    .card-img, .card-img-top {
        border-top-left-radius: 20px !important;
        border-top-right-radius: 20px !important;
        object-fit: cover;
        height: 220px;
    }

    .bxs-star{
        color: #D98B19;
    }

    .empty{
        font-size: 18px;
    }

    .sub_empty{
        font-size: 16px;
        color:#433E3B;
    }

    .empty_illustration{
        width: 300px;
    }

    .menu_desc{
        font-size: 24px;
        font-weight: bold;
    }

    .bs-item-container {
        z-index: 0;
    }

    .mailbox{
        height: 550px;
        margin-top: 5%;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        border-radius: 8px;
        padding: 5%;
        padding-top: 8%;
    }

    .con-container{
        background-color: white;;
        height:auto;
        padding-bottom:2%;
    }

    .i-field{
        border: none;
        border-bottom: 2px solid #D98B19 ;
        border-radius: 0;
    }

    .illustration{
        width: auto;
        margin: 5%;
    }

    .form-control:focus {
        box-shadow: none;
        border-color:#f2b31f;
    }

    .space{
        margin-top: 5%;
    }

    .modal-btn{
        background-color: #CE8719;
        border: none;
        color: white; 
        width: 20%;
    }

    .modal-btn:hover{
        background-color: #F19B1D;
        color: #f6f5f5;
        box-shadow: 0 4px 8px 0 rgb(241 137 29 / 20%), 0 6px 20px 0 rgb(241 137 29 / 19%);
    }

    .bx{
        font-size: 24px;
    }

    .icons{
        color: #BFBBBB;
    }

    .bx:hover{
        color: #ffbf00;
    }

    .associations{
        color: #BFBBBB;
    }

    .footer{
        height: 150px;
        background-color: #50504F;
        padding-bottom: 0;
        padding-top: 5%;
    }

    .f-wrapper{
        padding-bottom: 2%;
    }

    .branding{
        font-size: 24px;
        font-weight:bold;
        color: white;
    }

    </style>

</head>
    <body>
        <div class="d-flex justify-content-between align-items-center nav navbar">
            @foreach ($logo as $data)
            <div class="d-flex align-items-center p-1">
                    <span class="logo"><img src="{{url('/images/'. $data->logo_img) }}" class="logo" alt="Camp SaWings Logo" /></span>
                    <div class="branding">
                        <span>CAMP SA WINGS</span>
                    </div>       
            </div>
            @endforeach

            <div class="d-flex">
                <div>
                    <a class="link" href="{{('/home')}}">Home</a>
                    <a class="link active" href="{{('/menu')}}">Menu</a>
                    <a class="link" href="{{('/about')}}">About</a>
                    <a class="link" href="{{('/faqs')}}">FAQs</a>
                </div>
            </div>
        </div>


        <div class="banner-bg" style="background-image: url('/images/menu-accent.png')">
            <div class=" banner-content d-flex flex-column align-items-start justify-content-center">
                <div class="banner-wrapper d-flex align-self-center">
                    <div>
                        <span>OUR MENU</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="main-container">
            <!-- Start Best Sellers Div -->
            <div class="bs-container">
                
                <div class="container-heading-wrapper">
                    <span class="container-heading one">BEST</span>
                    <span class="container-heading two">SELLERS</span>
                </div>
            

                <div class="bs-item-container px-5">        
                    <div class="d-flex justify-content-left  px-5 flex-wrap py-2">
                    @foreach ($best_sellers as $bs)
                        <div class="px-3">
                            <div class="card" style="width: 18rem;">
                                <img class="card-img-top" src="{{ url ('/images/' . $bs->bs_image)}}" alt="Card image cap">
                                <div class="card-body ">
                                    <p class="menu_desc m-0">{{$bs->bs_desc}}</p>
                                    <p class="price" style="color: #D98B19;"><i class='bx bx-purchase-tag-alt'></i>{{$bs->bs_price}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div> 

                    @if ($best_sellers->isEmpty())
                    <div class="d-flex flex-column align-items-center">
                        <div>
                            <img src="{{ url('images/void.svg') }}" class="empty_illustration">
                        </div>

                        <div class="empty m-3">
                            <p>No results found.</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        <!-- End of Best Sellers Div -->

        <!-- Start Sulit Meals Div -->
        <div class="sm-container">
                
                <div class="container-heading-wrapper">
                    <span class="container-heading one">SULIT</span>
                    <span class="container-heading two">MEALS</span>
                </div>
            

                <div class="bs-item-container px-5">        
                    <div class="d-flex justify-content-left  px-5 flex-wrap py-2">
                    @foreach ($sulit_meals as $sm)
                        <div class="px-3">
                            <div class="card" style="width: 18rem;">
                                <img class="card-img-top" src="{{ url ('/images/' . $sm->sm_image)}}" alt="Card image cap">
                                <div class="card-body ">
                                    <p class="menu_desc m-0">{{$sm->sm_desc}}</p>
                                    <p class="price" style="color: #D98B19;"><i class='bx bx-purchase-tag-alt'></i>{{$sm->sm_price}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div> 

                    @if ($sulit_meals->isEmpty())
                    <div class="d-flex flex-column align-items-center">
                        <div>
                            <img src="{{ url('images/void.svg') }}" class="empty_illustration">
                        </div>

                        <div class="empty m-3">
                            <p>No results found.</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        <!-- End of Best Sellers Div -->

        <!-- Start Barkada Bundle Div -->
        <div class="bb-container">
                
                <div class="container-heading-wrapper">
                    <span class="container-heading one">BARKADA</span>
                    <span class="container-heading two">BUNDLES</span>
                </div>
            

                <div class="bs-item-container px-5">        
                    <div class="d-flex justify-content-left  px-5 flex-wrap py-2">
                    @foreach ($barkada_bundle as $bb)
                        <div class="px-3">
                            <div class="card" style="width: 18rem;">
                                <img class="card-img-top" src="{{ url ('/images/' . $bb->bb_image)}}" alt="Card image cap">
                                <div class="card-body ">
                                    <p class="menu_desc m-0">{{$bb->bb_desc}}</p>
                                    <p class="price" style="color: #D98B19;"><i class='bx bx-purchase-tag-alt'></i>{{$bb->bb_price}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div> 

                    @if ($barkada_bundle->isEmpty())
                    <div class="d-flex flex-column align-items-center">
                        <div>
                            <img src="{{ url('images/void.svg') }}" class="empty_illustration">
                        </div>

                        <div class="empty m-3">
                            <p>No results found.</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        <!-- End of Best Sellers Div -->
        </div>

        <!-- Footer -->
        <div class="footer">
            <div class="d-flex justify-content-center">
                @foreach ($social as $link)
                <div class="icons">
                    <a href="{{$link->fb_link}}" style="color:#BFBBBB;" target="_blank"><i class='bx bxl-facebook-square'></i></a>
                </div>
                <div class="icons">
                    <a href="{{$link->instagram_link}}" style="color:#BFBBBB;" target="_blank"><i class='bx bxl-instagram'></i></a>
                </div>
                <div class="icons">
                    <a href="{{'mailto:'.$link->gmail_link}}" style="color:#BFBBBB;" target="_blank"><i class='bx bxl-gmail'></i></a>
                </div>
                @endforeach 
            </div>
            <div class="d-flex justify-content-center">
                <?php
                    $year = date('Y');
                ?>
                <div class="associations">
                    <span>@<?php echo $year ?>- Accezen. All Right Reserved. Designed and Developed by Jan Patrick Delacruz Urbano</span>
                </div>
            </div>
        </div>
        <!-- End of Footer -->


    </body>
</html>