<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | Home</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
<link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap');
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Montserrat', sans-serif;
    }

    html {
        scroll-behavior: smooth;
    }

    .sidenav{
        background-color: #F0F4F7;
        height: 95vh;
        border-radius: 0 36px 0 0;
        z-index: 1;
        margin-top: 2%;
        position: fixed;
    }

    .row{
        --bs-gutter-x: 0;
    }

    .accent{
        padding: 0px;
        height: 30vh;
        background-color: black;
        margin-top: 4.7%;
    }

    .admin-icon{
        color: white;
        font-size: 32pt;
        z-index: 3;
    }

    .navbar{
        position: fixed;
        width: 100%;
        display: flex;
        justify-content: flex-end;
        align-items: center;
        z-index: 3; 
    }

    .avatar{
        position: relative;
        display: inline-block;
        transition: all 0.3s ease 0s;
    }

    .dropdown-content{
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        right: 43px;       
    }

    .dropdown-content a{
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        transition: all 0.3s ease 0s;
    }

    .dropdown-content a:hover{
        background-color: #ddd;
        border-radius: 8px;
        transition: all 0.3s ease 0s;
    }

    .avatar:hover .dropdown-content {
        display: block;
        border-radius: 8px;
        transition: all 0.3s ease 0s;
    }

    .avatar:hover {
        color: #3e8e41;
        border-radius: 8px;
        z-index: 10;
    }

    .break{
        height: 1.5px;
    }

    .main{
        position: absolute;
        z-index: 0;
    }

    .logo img{
        width: 40%;
    }

    .logo{
        display: flex;
        justify-content: center;
        margin-top: 2%;
    }

    .burger{
        color:  #A7B9C6;
    }

    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
        color: var(--bs-nav-pills-link-active-color);
        background-color: #212529;
    }

    .nav-link{
        color: #433E3B;
    }


    .nav .nav-item .nav-link:hover{
        color: #CE8719;
    }

    .nav-banner{
        display: flex;
        justify-content: center;
    }

    .bot-nav{
        position: fixed;
        bottom:1%;
        background-color: #212529;
        color: white;
        height: 9vh;
        width: 16.6%;
        border-radius: 10px 10px 0 0;
    }

    .lbl{
        display: flex;
        justify-content: center;
        font-weight: bold;
        margin: 0;
    }
    .sub{
        display: flex;
        justify-content: center;
        font-size: 9px;
    }

    .main-container{
        background-color: #fefefe;
        height: auto;
        padding-bottom:5%;
        margin: 1%;
        border-radius: 20px;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        z-index: -3;
    }

    .content-img {
        display: flex;
        width: 450px;
        height: 250px;
        float: left;
        margin: 3%;
        border-radius: 10px;
        justify-content: center;
        object-fit: cover;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        
    }

    .card{
        
        margin: 2%;
        margin-top: 5%;
        border-radius: 10px; 
        z-index: 0;
        
    }

    .content-img img{
        width: 450px;
        height: 250px;
        border-radius: 10px;
        object-fit: cover;
    }


    .banner-edit{
        position: absolute;
        right: 3%;
        width: 12%;
        padding-top: 12%;
        color: white;
        background-color: #CE8719;
        place-content: end;
        border: none;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        border-radius: 8px;
    }

    .banner-edit:hover{
        background-color: #F19B1D;
        color: #f6f5f5;
        box-shadow: 0 4px 8px 0 rgb(241 137 29 / 20%), 0 6px 20px 0 rgb(241 137 29 / 19%);
    }

    .banner-heading{
        margin-top: 2.5%;
        margin-bottom: 0;
        font-weight: bold;
        font-size: 36px;
    }

    .banner-sub-heading{
        
        font-size: 24px;
    }

    .section-heading{
        padding-left: 2.5%;
        padding-top: 3%;
        font-size: 36px;
        font-weight: bold;
    }

    .close{
        font-size: 36px;
        border: none;
        background-color: transparent;
    }

    .modal-open {
        overflow: inherit !important;
    }

    .modal-btn{
        background-color: #CE8719;
        border: none;
        color: white; 
        width: 20%;
    }

    .modal-btn:hover{
        background-color: #F19B1D;
        color: #f6f5f5;
        box-shadow: 0 4px 8px 0 rgb(241 137 29 / 20%), 0 6px 20px 0 rgb(241 137 29 / 19%);
    }

</style>

</head>
<body>
    <div class="main_container">
        <div class="row">
            <div class="col-sm-2 sidenav">
            @foreach ($logo as $data)
                <div class="logo">
                    <span class="logo"><img src="{{url('/images/'. $data->logo_img) }}" alt="Camp SaWings Logo" /></span>
                </div>
                @endforeach
                <br>
                <div class="nav-banner">
                    <span>Manage Sites Pages</span>
                </div>
                <div class="selection mt-3">
                    <ul class="nav nav-pills flex-column mb-auto p-2">
                        <li class="nav-item">
                            <a href="{{('/admin/dashboard')}}" class="nav-link active" aria-current="page">
                            <i class='bx bx-home-alt'></i>
                            Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/menu')}}" class="nav-link">
                            <i class='bx bx-food-menu'></i>
                            Menus
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/faqs')}}" class="nav-link">
                            <i class='bx bx-game' ></i>
                            FAQ's
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/about')}}" class="nav-link">
                            <i class='bx bx-bulb'></i>
                            About
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/settings')}}" class="nav-link">
                            <i class='bx bx-cog'></i>
                            Settings
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="bot-nav">
                    <p class="pt-3 lbl">Administrative Account</p>
                    <span class="sub">CampSaWings Content Management System</span>
                </div>
            </div>
            <div class="col-sm-12 main">
                <nav>
                    <div class="navbar navbar-dark bg-dark navbar-expand-sm"">
                        <div class="label-admin">
                            <span class="text-light ml-4">Admin&nbsp;&nbsp;&nbsp;</span>
                        </div>
                        <div>

                        </div>
                        <div class="avatar">
                            <i class="fas fa-user-circle admin-icon"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="dropdown">
                                <div class="dropdown-content">
                                    <span><a href="{{'/login/admin'}}">Logout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-sign-out"></i></a></span>
                                </div>
                            </div>
                        </div>     
                    </div>  
                </nav>
            <div class="break">
            </div>
            <div class="row">
                <div class="col-sm-2">

                </div>
                <div class="col-sm-10">
                    <div class="accent bg-dark">
                        <h2 class="p-4 burger my-1"><i class='bx bx-home-alt'></i>Home</h2>
                        <div class="main-container">
                            <div class="home-banner-container">
                                <div class="section-heading">
                                    <span>Home Banner</span>
                                </div>

                                <script>
                               
                                @if(Session::has('message'))
                                toastr.options =
                                {
                                    "closeButton" : false,
                                    "progressBar" : true,
                                    "positionClass": "toast-bottom-right",
                                    "timeOut": "2000",
                                }
                                        toastr.success("{{ session('message') }}");
                                @endif

                                @if(Session::has('error'))
                                toastr.options =
                                {
                                    "closeButton" : false,
                                    "progressBar" : true,
                                    "positionClass": "toast-bottom-right",
                                    "timeOut": "2000",
                                }
                                        toastr.error("{{ session('error') }}");
                                @endif

                                </script>


                               
                                <div class="btn-container">
                                    <button class = "banner-edit py-2" data-bs-toggle="modal" data-bs-target="#banner-edit"><i class='bx bx-edit-alt'></i>&nbsp;&nbsp;Update</button>
                                </div>
                             
                                

                                @foreach ($home_page as $data)
                                <div class="card py-4">
                                    <br>
                                    
                                    <div class="card-content">
                                        <div class="content-img">
                                            <img src="{{ url ('/images/' . $data->banner_img)}}">
                                        </div>
                                    
                                        <div class="banner-heading text-bold">
                                            <p class="banner-heading">{{$data->heading}}</p>
                                        </div>
                                        <div class="banner-sub-heading text-bold">
                                            <p style="margin-bottom: 0 !important;">{{$data->sub_heading}}</p>
                                        </div>

                                        <div class="banner-sub-heading text-bold" style="color:#CE8719;">
                                            <p>{{$data->time}}</p>
                                        </div>

                                        <div class="cor-d">
                                            <input type="color" value="{{$data->color}}" disabled>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modals -->
    @foreach ($home_page as $data)
    <div class="modal fade" id="banner-edit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Home Banner</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mx-3">

                        <form action="/edit-banner" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="formFile" class="form-label">Select Banner Image</label>
                                <input class="form-control" type="file" name= "banner_img" id="formFile" accept="image/*" required>
                            </div>

                            <div class="form-group mt-3 mt-3">
                                <label class="form-label">Banner Heading</label>
                                <input type="text" class="form-control" name="banner-heading" value="{{$data->heading}}" required>
                            </div>

                            <div class="form-group mt-3">
                                <label class="form-label">Banner Sub-Heading</label>
                                <input type="text" class="form-control" name="banner-sub-heading" value="{{$data->sub_heading}}" required>
                            </div>

                            <div class="d-flex justify-content-between m-3">
                                <div class="form-group mt-3 col-sm-10">
                                    <label class="form-label">Operating Hours</label>
                                    <input type="text" class="form-control" name="time" value="{{$data->time}}" required>
                                </div>

                                <div class="form-group mt-3 ">
                                    <label class="form-label ">Text Color</label>
                                    <input type="color" class="form-control form-control-color " name="color" value="{{$data->color}}" required>
                                </div>

                            </div>
                           

                            
                            

                    </div>
                </div>
                <div class="modal-footer mt-3">
                        <button type="submit" id="edit-banner" class="btn px-3 modal-btn">Save</button>
                </div>
                    </form>
            </div>
        </div>
    </div>
    @endforeach
</body>
</html>