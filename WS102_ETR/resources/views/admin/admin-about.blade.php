<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | About</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
<link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap');
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Montserrat', sans-serif;
    }

    html {
        scroll-behavior: smooth;
    }

    .sidenav{
        background-color: #F0F4F7;
        height: 95vh;
        border-radius: 0 36px 0 0;
        z-index: 1;
        margin-top: 2%;
        position: fixed;
    }

    .row{
        --bs-gutter-x: 0;
    }

    .accent{
        padding: 0px;
        height: 30vh;
        background-color: black;
        margin-top: 4.7%;
    }

    .admin-icon{
        color: white;
        font-size: 32pt;
        z-index: 3;
    }

    .navbar{
        position: fixed;
        width: 100%;
        display: flex;
        justify-content: flex-end;
        align-items: center;
        z-index: 3; 
    }

    .avatar{
        position: relative;
        display: inline-block;
        transition: all 0.3s ease 0s;
    }

    .dropdown-content{
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        right: 43px;       
    }

    .dropdown-content a{
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        transition: all 0.3s ease 0s;
    }

    .dropdown-content a:hover{
        background-color: #ddd;
        border-radius: 8px;
        transition: all 0.3s ease 0s;
    }

    .avatar:hover .dropdown-content {
        display: block;
        border-radius: 8px;
        transition: all 0.3s ease 0s;
    }

    .avatar:hover {
        color: #3e8e41;
        border-radius: 8px;
        z-index: 10;
    }

    .break{
        height: 1.5px;
    }

    .main{
        position: absolute;
        z-index: 0;
    }

    .logo img{
        width: 40%;
    }

    .logo{
        display: flex;
        justify-content: center;
        margin-top: 2%;
    }

    .burger{
        color:  #A7B9C6;
    }

    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
        color: var(--bs-nav-pills-link-active-color);
        background-color: #212529;
    }

    .nav-link{
        color: #433E3B;
    }


    .nav .nav-item .nav-link:hover{
        color: #CE8719;
    }

    .nav-banner{
        display: flex;
        justify-content: center;
    }

    .bot-nav{
        position: fixed;
        bottom:1%;
        background-color: #212529;
        color: white;
        height: 9vh;
        width: 16.6%;
        border-radius: 10px 10px 0 0;
    }

    .lbl{
        display: flex;
        justify-content: center;
        font-weight: bold;
        margin: 0;
    }
    .sub{
        display: flex;
        justify-content: center;
        font-size: 9px;
    }

    .main-container{
        background-color: #fefefe;
        height: auto;
        padding-bottom:5%;
        margin: 1%;
        border-radius: 20px;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        z-index: -3;
    }

    .content-img {
        display: flex;
        width: 200px;
        height: 120px;
        float: left;
        margin: 3%;
        padding: 3px;
        border: solid #CE8719;
        border-radius: 200%;
        justify-content: center;
    }

    .card{
        margin: 2%;
        margin-top: 5%;
        border-radius: 10px; 
        z-index: 0;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
    }

    .content-img img{
        max-width: 100%;
        height: auto;
        border-radius: 10px;
    }


    .banner-edit{
        position: absolute;
        right: 3%;
        width: 12%;
        padding-top: 12%;
        color: white;
        background-color: #CE8719;
        place-content: end;
        border: none;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        border-radius: 8px;
    }

    .banner-edit:hover{
        background-color: #F19B1D;
        color: #f6f5f5;
        box-shadow: 0 4px 8px 0 rgb(241 137 29 / 20%), 0 6px 20px 0 rgb(241 137 29 / 19%);
    }

    .banner-heading{
        margin-top: 2.5%;
        margin-bottom: 0;
        font-weight: bold;
        font-size: 36px;
    }

    .banner-sub-heading{
        
        font-size: 24px;
    }

    .section-heading{
        padding-left: 2.5%;
        padding-top: 3%;
        font-size: 36px;
        font-weight: bold;
    }

    .close{
        font-size: 36px;
        border: none;
        background-color: transparent;
    }

    .modal-open {
        overflow: inherit !important;
    }

    .modal-btn{
        background-color: #CE8719;
        border: none;
        color: white; 
        width: 20%;
    }

    .modal-btn:hover{
        background-color: #F19B1D;
        color: #f6f5f5;
        box-shadow: 0 4px 8px 0 rgb(241 137 29 / 20%), 0 6px 20px 0 rgb(241 137 29 / 19%);
    }

    .faqs_div{
        margin-top: auto;
        padding: 5%;
    }

    .question_container{
        font-size: 18px;
        font-weight:200;
        color:#CE8719;
    }

    .accordion-flush .accordion-item .accordion-button {
        border-radius: 8px;
    }

    .accordion-button:not(.collapsed) {
        color: #f7c300cc;
        background-color: #ecbc6130;
        box-shadow: inset 0 calc(var(--bs-accordion-border-width) * -1) 0 var(--bs-accordion-border-color);
    }

    .accordion-button:focus{
        color: #f7c300cc;
        background-color: #ecbc6130;
        box-shadow: inset 0 calc(var(--bs-accordion-border-width) * -1) 0 var(--bs-accordion-border-color);
        outline: none;
    }

    .bu{
        margin-top: 10px;
    }

    .but{
        border-style: none;
        background-color: transparent;
    }

    .but:hover{
        color: #D98B19;
        border: none !important;
    }

    .empty{
        font-size: 18px;
    }

    .empty_con{
       margin-top: -80px;
    }

    .sub_empty{
        font-size: 16px;
        color:#433E3B;
    }

    .empty_illustration{
        width: 300px;
    }

    .modal-open {
        overflow: inherit !important;
    }

    .card-img{
        border-radius: 50%;
        object-fit: cover;
        height: 180px;
        width: 180px;
        border: solid #D98B19;
    }

    .position{
        font-style: italic;
        color:#433E3B;
    }

    .name{
        color: #D98B19;
    }

    .home-banner-container-1{
        height: auto;
    }

    .p-5 {
        padding: 1.5rem !important;
    }
    
    


</style>

</head>
<body>
    <div class="main_container">
        <div class="row">
            <div class="col-sm-2 sidenav">
            @foreach ($logo as $data)
                <div class="logo">
                    <span class="logo"><img src="{{url('/images/'. $data->logo_img) }}" alt="Camp SaWings Logo" /></span>
                </div>
                @endforeach
                <br>
                <div class="nav-banner">
                    <span>Manage Sites Pages</span>
                </div>
                <div class="selection mt-3">
                    <ul class="nav nav-pills flex-column mb-auto p-2">
                        <li class="nav-item">
                            <a href="{{('/admin/dashboard')}}" class="nav-link">
                            <i class='bx bx-home-alt'></i>
                            Home
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{('/admin/menu')}}" class="nav-link">
                            <i class='bx bx-food-menu'></i>
                            Menus
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/faqs')}}" class="nav-link">
                            <i class='bx bx-game' ></i>
                            FAQ's
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/about')}}" class="nav-link active" aria-current="page">
                            <i class='bx bx-bulb'></i>
                            About
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/settings')}}" class="nav-link">
                            <i class='bx bx-cog'></i>
                            Settings
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="bot-nav">
                    <p class="pt-3 lbl">Administrative Account</p>
                    <span class="sub">CampSaWings Content Management System</span>
                </div>
            </div>
            <div class="col-sm-12 main">
                <nav>
                    <div class="navbar navbar-dark bg-dark navbar-expand-sm"">
                        <div class="label-admin">
                            <span class="text-light ml-4">Admin&nbsp;&nbsp;&nbsp;</span>
                        </div>
                        <div>

                        </div>
                        <div class="avatar">
                            <i class="fas fa-user-circle admin-icon"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="dropdown">
                                <div class="dropdown-content">
                                    <span><a href="{{'/login/admin'}}">Logout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-sign-out"></i></a></span>
                                </div>
                            </div>
                        </div>     
                    </div>  
                </nav>
            <div class="break">
            </div>
            <div class="row">
                <div class="col-sm-2">

                </div>
                <div class="col-sm-10">
                    <div class="accent bg-dark">
                        <h2 class="p-4 burger"><i class='bx bx-bulb'></i>About</h2>
                        <div class="main-container">
                            <div class="home-banner-container-1">
                                <div class="section-heading">
                                    <span>Story</span>
                                </div>

                                @foreach($story as $st)
                                <form action="/update-story" method="POST">
                                @csrf
                                    <div class="btn-container p-5">
                                        <button type="submit" class = "banner-edit py-2" ><i class='bx bx-edit-alt'></i>&nbsp;&nbsp;Update</button>
                                    </div>

                                    <?php
                                        $output = $st->story;
                                    ?>
                                    

                                    <div class="form-group p-5">
                                        <textarea class="form-control story_area mt-3" name="story" id="story" rows=15 ><?php echo $output;?></textarea>
                                    </div>  
                                
                                </form>
                                @endforeach

                                <script src={{ asset('ckeditor/ckeditor.js') }}></script>
                                <script>
                                    ClassicEditor
                                        .create( document.querySelector( '#story' ) )
                                        .catch( error => {
                                            console.error( error );
                                        } );
                                </script>
                            </div>
                            <!-- end story -->


                            <!-- start org -->
                            <div class="home-banner-container">
                                <div class="section-heading">
                                    <span>Organization</span>
                                </div>

                                <div class="btn-container">
                                    <button class = "banner-edit py-2" data-bs-toggle="modal" data-bs-target="#org-add"><i class='bx bx-plus'></i>&nbsp;&nbsp;Add</button>
                                </div>
                               
                                <div class="d-flex justify-content-left  px-4 flex-wrap py-5">
                                    @foreach ($org as $data)
                                    <div class="card" style="width: 15rem;">
                                        
                                        <div class="card-body d-flex justify-content-center">
                                            <img class="card-img" src="{{ url ('/images/' . $data->org_img)}}">
                                            
                                        </div>
                                        <div class="name d-flex justify-content-center ">
                                            <center><p class="name m-0" style="font-size: 18px; font-weight: bold; ">{{$data->name}} </p></center>  
                                        </div>

                                        <div class="position d-flex justify-content-center ">
                                            <center>
                                                <p class=" position ">
                                                    {{$data->position}} 
                                                </p>
                                            </center>
                                        </div>

                                        <div class="card-footer d-flex justify-content-end">
                                            <form action="/delete-org-item" method="POST">
                                                @csrf
                                                <input type="hidden" name="org_id" value="{{$data->org_id}}">
                                                <button onclick="return confirm('Are you sure you want to delete this item?');" type="submit" class="but"><i class='bx bxs-trash-alt bx-tada-hover  bx-border-circle' style="font-size: 22px;" ></i></button>
                                            </form>
                                        </div>
                                            
                                    </div>
                                    @endforeach
                                </div>

                                @if ($org->isEmpty())
                                <div class="d-flex flex-column align-items-center empty_con">
                                    <div>
                                        <img src="{{ url('images/empty1.png') }}" class="empty_illustration">
                                    </div>
                                    <div class="empty m-0">
                                        <p>No results found.</p>
                                    </div>
                                    <div class="empty">
                                        <p class="sub_empty">Click the Add button to add an item</p>
                                    </div>
                                </div>
                                @endif
                                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modals Story-->
    @foreach ($story as $data)
    <div class="modal fade" id="story-update" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Add FAQs</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mx-3">

                        <form action="/update-story" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Write your Story</label>
                                <textarea class="form-control story_area mt-3" name="story" rows=15 >{{$data->story}}</textarea>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                        <button type="submit" class="btn px-3 modal-btn">Save</button>
                </div>
                    </form>
            </div>
        </div>
    </div>
    @endforeach
    <!-- end modal story -->

    <!-- Modals Add Org-->
    <div class="modal fade" id="org-add" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Add Organization</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mx-3">

                        <form action="/add-org" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mt-3 ">
                                <label class="form-label">Name</label>
                                <input type="text" class="form-control" name="name" value="" required>
                            </div>

                            <div class="form-group mt-3  mb-3">
                                <label class="form-label">Position</label>
                                <input type="text" class="form-control" name="position" value="" required>
                            </div>

                            <div class="form-group mb-4">
                                <label for="formFile" class="form-label">Upload Profile</label>
                                <input class="form-control" type="file" name= "org_img" id="formFile" accept="image/*" required>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button type="submit"  class="btn px-3 modal-btn">Save</button>
                </div>
                    </form>
            </div>
        </div>
    </div>
    <!-- End of Modal org -->
    <script>
                               
        @if(Session::has('message'))
        toastr.options =
        {
            "closeButton" : false,
            "progressBar" : true,
            "positionClass": "toast-bottom-right",
            "timeOut": "2000",
        }
                toastr.success("{{ session('message') }}");
        @endif

        @if(Session::has('error'))
        toastr.options =
        {
            "closeButton" : false,
            "progressBar" : true,
            "positionClass": "toast-bottom-right",
            "timeOut": "2000",
        }
                toastr.error("{{ session('error') }}");
        @endif

    </script>

</body>
</html>