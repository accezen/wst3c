<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | Login</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap');
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Montserrat', sans-serif;
    }

    .main_container{
        margin-top: 3%;
        display: flex;
        justify-content:center;
    }

    .logo img{
        width: 20%;
    }

    .logo{
        display: flex;
        justify-content: center;
        margin-top: 5%;

    }

    .login-heading{
        color: #D98B19;
        font-size: 36px;
        font-weight: bold;
    }

    .login-heading2{
        color: #50504F;
        font-size: 36px;
        font-weight: bold;
    }

    .login-heading-wrapper{
        display: flex;
        justify-content: center;
    }

    .greetings{
        color: #50504F;
        display: flex;
        justify-content: center;
        margin-bottom: 5%;
    }

    .form-container{
        margin: 0% 20%;
    }

    .form-control{   
        border: none;
        border: 1px solid #D98B19;
        padding: 2%;
    }

    .form-control:focus{
        border-color: #D98B19;
        box-shadow: 0 0 5px #D98B19;
    }

    .btn-container{
        display: flex;
        justify-content: center;
    }

    .login-button{
        font-size: 14pt;
        background-color: #D98B19;
        width: 70%;
        color: #FFFFFF;
    }

    .login-button:hover{
        font-size: 14pt;
        background-color: #F19B1D;
        width: 70%;
        color: #F5F5F5;
    }

</style>
</head>
<body>
    <div class="main_container">
        <div class="form_wrapper">
            <div class="logo_container">
                <span class="logo"><img src="/images/log.png" alt="Camp SaWings Logo" /></span>
                <div class="login-heading-wrapper mt-3">
                    <span class="login-heading">Admin&nbsp;</span>
                    <span class="login-heading2"> Login</span>
                </div>
                <div class="greetings mt-3">
                    <h4>Welcome Back!</h4>
                </div>
                <div class="form-container">
                   
                    <form action="{{('/login/admin')}}" method="POST" >
                    @csrf
                        <div class="form-group">
                            <input type="email" class="form-control"  placeholder="Enter email" name="email" value="{{old ('email')}}">
                        </div>
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                        <div class="form-group mt-4">
                            <input type="password" class="form-control" placeholder="Password" name="password" value="{{old ('password')}}">
                        </div>
                        @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                        <div class="btn-container mt-5">
                            <button type="submit" class="btn login-button">LOGIN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script> 

        @if(Session::has('error'))
        toastr.options =
        {
            "closeButton" : false,
            "progressBar" : true,
            "positionClass": "toast-top-right",
            "timeOut": "2000",
        }
                toastr.error("{{ session('error') }}");
        @endif

    </script>
</body>
</html>