<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | Settings</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
<link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap');
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Montserrat', sans-serif;
    }

    html {
        scroll-behavior: smooth;
    }

    .sidenav{
        background-color: #F0F4F7;
        height: 95vh;
        border-radius: 0 36px 0 0;
        z-index: 1;
        margin-top: 2%;
        position: fixed;
    }

    .row{
        --bs-gutter-x: 0;
    }

    .accent{
        padding: 0px;
        height: 30vh;
        background-color: black;
        margin-top: 4.7%;
    }

    .admin-icon{
        color: white;
        font-size: 32pt;
        z-index: 3;
    }

    .navbar{
        position: fixed;
        width: 100%;
        display: flex;
        justify-content: flex-end;
        align-items: center;
        z-index: 3; 
    }

    .avatar{
        position: relative;
        display: inline-block;
        transition: all 0.3s ease 0s;
    }

    .dropdown-content{
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        right: 43px;       
    }

    .dropdown-content a{
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        transition: all 0.3s ease 0s;
    }

    .dropdown-content a:hover{
        background-color: #ddd;
        border-radius: 8px;
        transition: all 0.3s ease 0s;
    }

    .avatar:hover .dropdown-content {
        display: block;
        border-radius: 8px;
        transition: all 0.3s ease 0s;
    }

    .avatar:hover {
        color: #3e8e41;
        border-radius: 8px;
        z-index: 10;
    }

    .break{
        height: 1.5px;
    }

    .main{
        position: absolute;
        z-index: 0;
    }

    .logo img{
        width: 40%;
    }

    .logo{
        display: flex;
        justify-content: center;
        margin-top: 2%;
    }

    .burger{
        color:  #A7B9C6;
    }

    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
        color: var(--bs-nav-pills-link-active-color);
        background-color: #212529;
    }

    .nav-link{
        color: #433E3B;
    }


    .nav .nav-item .nav-link:hover{
        color: #CE8719;
    }

    .nav-banner{
        display: flex;
        justify-content: center;
    }

    .bot-nav{
        position: fixed;
        bottom:1%;
        background-color: #212529;
        color: white;
        height: 9vh;
        width: 16.6%;
        border-radius: 10px 10px 0 0;
    }

    .lbl{
        display: flex;
        justify-content: center;
        font-weight: bold;
        margin: 0;
    }
    .sub{
        display: flex;
        justify-content: center;
        font-size: 9px;
    }

    .main-container{
        background-color: #fefefe;
        height: auto;
        padding-bottom:5%;
        margin: 1%;
        border-radius: 20px;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        z-index: -3;
    }

    .content-img {
        display: flex;
        width: 200px;
        height: 120px;
        float: left;
        margin: 3%;
        padding: 3px;
        border: solid #CE8719;
        border-radius: 10px;
        justify-content: center;
    }

    .card{
        margin: 2%;
        margin-top: 5%;
        border-radius: 10px; 
        z-index: 0;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
    }

    .content-img img{
        max-width: 100%;
        height: auto;
        border-radius: 10px;
    }


    .banner-edit{
        position: absolute;
        right: 3%;
        width: 12%;
        padding-top: 12%;
        color: white;
        background-color: #CE8719;
        place-content: end;
        border: none;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        border-radius: 8px;
    }

    .banner-edit:hover{
        background-color: #F19B1D;
        color: #f6f5f5;
        box-shadow: 0 4px 8px 0 rgb(241 137 29 / 20%), 0 6px 20px 0 rgb(241 137 29 / 19%);
    }

    .banner-heading{
        margin-top: 2.5%;
        margin-bottom: 0;
        font-weight: bold;
        font-size: 36px;
    }

    .banner-sub-heading{
        
        font-size: 24px;
    }

    .section-heading{
        padding-left: 2.5%;
        padding-top: 3%;
        font-size: 36px;
        font-weight: bold;
    }

    .close{
        font-size: 36px;
        border: none;
        background-color: transparent;
    }

    .modal-open {
        overflow: inherit !important;
    }

    .modal-btn{
        background-color: #CE8719;
        border: none;
        color: white; 
        width: 10%;
    }

    .modal-btn:hover{
        background-color: #F19B1D;
        color: #f6f5f5;
        box-shadow: 0 4px 8px 0 rgb(241 137 29 / 20%), 0 6px 20px 0 rgb(241 137 29 / 19%);
    }

    .faqs_div{
        margin-top: auto;
        padding: 5%;
    }

    .question_container{
        font-size: 18px;
        font-weight:200;
        color:#CE8719;
    }

    .accordion-flush .accordion-item .accordion-button {
        border-radius: 8px;
    }

    .accordion-button:not(.collapsed) {
        color: #f7c300cc;
        background-color: #ecbc6130;
        box-shadow: inset 0 calc(var(--bs-accordion-border-width) * -1) 0 var(--bs-accordion-border-color);
    }

    .accordion-button:focus{
        color: #f7c300cc;
        background-color: #ecbc6130;
        box-shadow: inset 0 calc(var(--bs-accordion-border-width) * -1) 0 var(--bs-accordion-border-color);
        outline: none;
    }

    .bu{
        margin-top: 10px;
    }

    .but{
        border-style: none;
        background-color: transparent;
    }

    .but:hover{
        color: #D98B19;
        border: none !important;
    }

    .empty{
        font-size: 18px;
    }

    .empty_con{
       margin-top: -80px;
    }

    .sub_empty{
        font-size: 16px;
        color:#433E3B;
    }

    .empty_illustration{
        width: 300px;
    }

    .modal-open {
        overflow: inherit !important;
    }

    .card-img{
        border-radius: 50%;
        object-fit: cover;
        height: 180px;
        width: 180px;
        border: solid #D98B19;
    }

    .position{
        font-style: italic;
        color:#433E3B;
    }

    .name{
        color: #D98B19;
    }

    .social-links{
        margin-top: -5%;
    }

    /* .btn-wrapper{
        margin-left: 76%;;
    }
     */
    
</style>

</head>
<body>
    <div class="main_container">
        <div class="row">
            <div class="col-sm-2 sidenav">
                @foreach ($logo as $data)
                <div class="logo">
                    <span class="logo"><img src="{{url('/images/'. $data->logo_img) }}" alt="Camp SaWings Logo" /></span>
                </div>
                @endforeach
                <br>
                <div class="nav-banner">
                    <span>Manage Sites Pages</span>
                </div>
                <div class="selection mt-3">
                    <ul class="nav nav-pills flex-column mb-auto p-2">
                        <li class="nav-item">
                            <a href="{{('/admin/dashboard')}}" class="nav-link">
                            <i class='bx bx-home-alt'></i>
                            Home
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{('/admin/menu')}}" class="nav-link">
                            <i class='bx bx-food-menu'></i>
                            Menus
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/faqs')}}" class="nav-link">
                            <i class='bx bx-game' ></i>
                            FAQ's
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/about')}}" class="nav-link">
                            <i class='bx bx-bulb'></i>
                            About
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{('/admin/settings')}}" class="nav-link active" aria-current="page">
                            <i class='bx bx-cog'></i>
                            Settings
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="bot-nav">
                    <p class="pt-3 lbl">Administrative Account</p>
                    <span class="sub">CampSaWings Content Management System</span>
                </div>
            </div>
            <div class="col-sm-12 main">
                <nav>
                    <div class="navbar navbar-dark bg-dark navbar-expand-sm"">
                        <div class="label-admin">
                            <span class="text-light ml-4">Admin&nbsp;&nbsp;&nbsp;</span>
                        </div>
                        <div>

                        </div>
                        <div class="avatar">
                            <i class="fas fa-user-circle admin-icon"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="dropdown">
                                <div class="dropdown-content">
                                    <span><a href="{{'/login/admin'}}">Logout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-sign-out"></i></a></span>
                                </div>
                            </div>
                        </div>     
                    </div>  
                </nav>
            <div class="break">
            </div>
            <div class="row">
                <div class="col-sm-2">

                </div>
                <div class="col-sm-10">
                    <div class="accent bg-dark">
                        <h2 class="p-4 burger"><i class='bx bx-cog'></i>Settings</h2>
                        <div class="main-container">
                            <!-- Manage Account Container -->
                            <div class="home-banner-container">
                                <div class="section-heading">
                                    <span>Manage Account</span>
                                </div>

                                <div class="pass-setting-wrapper p-5">
                                    <form action="/update-password" method="POST">
                                        @csrf
                                        <div class="mb-3">
                                            <label class="form-label">Nominate New Password</label>
                                            <input type="password" class="form-control" name="c_pass" required>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Confirm Password</label>
                                            <input type="password" class="form-control" name="password" required>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                        <div class="div-wrapper d-flex justify-content-end">
                                            <button type="submit" class="btn modal-btn"><i class='bx bx-edit-alt'></i>&nbsp;&nbsp;&nbsp;Update</button>
                                        </div>    
                                    </form>
                                </div>
                            </div>
                            <!--End Manage Account-->

                            <!-- Start Social Links -->
                            <div class="home-banner-container social-links">
                                <div class="section-heading">
                                    <span>Social Links</span>
                                </div>

                                @foreach ($social as $data)
                                <div class="card m-5">
                                    <div class="card-header" style="font-size: 24px;">
                                        <span style="color:#CE8719;"><i class='bx bxl-facebook' style="color:#CE8719;"></i>&nbsp;&nbsp;Facebook</span>
                                    </div>
                                    <div class="card-body d-flex justify-content-between" style="font-size: 18px;">
                                        <div class="link-wrapper">
                                            <span>
                                                &nbsp;&nbsp;&nbsp;&nbsp;<i class='bx bx-link' style="color:#CE8719;" ></i>
                                                <a href="{{'https://'.$data->fb_link}}" style="color:#212529; text-decoration:none;" target="_blank">
                                                {{$data->fb_link}}
                                                </a>
                                            </span>
                                        </div>
                                        <div class="btn-wrapper">
                                            <button class="but mx-2 "><i class='bx bx-pencil bx-tada-hover bx-border-circle' style="font-size: 22px;" data-bs-toggle="modal" data-bs-target="#edit-fb"></i></button>
                                        </div> 
                                    </div>
                                </div>
                                @endforeach
                                @foreach ($social as $data)
                                <div class="card m-5">
                                    <div class="card-header" style="font-size: 24px;">
                                        <span style="color:#CE8719;"><i class='bx bxl-instagram' style="color:#CE8719;"></i>&nbsp;&nbsp;Instagram</span>
                                    </div>
                                    <div class="card-body d-flex justify-content-between" style="font-size: 18px;">
                                        <div class="link-wrapper">
                                            <span>
                                                &nbsp;&nbsp;&nbsp;&nbsp;<i class='bx bx-link' style="color:#CE8719;" ></i>
                                                <a href="{{'https://'.$data->instagram_link}}" style="color:#212529; text-decoration:none;" target="_blank">
                                                {{$data->instagram_link}}
                                                </a>
                                            </span>
                                        </div>
                                        <div class="btn-wrapper">
                                            <button class="but mx-2 "><i class='bx bx-pencil bx-tada-hover bx-border-circle' style="font-size: 22px;" data-bs-toggle="modal" data-bs-target="#edit-instagram"></i></button>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @foreach ($social as $data)
                                <div class="card m-5">
                                    <div class="card-header" style="font-size: 24px;">
                                        <span style="color:#CE8719;"><i class='bx bxl-gmail' style="color:#CE8719;"></i>&nbsp;&nbsp;Gmail</span>
                                    </div>
                                    <div class="card-body d-flex justify-content-between" style="font-size: 18px;">
                                    
                                        <div class="link-wrapper">
                                            </a>
                                            <span>
                                                &nbsp;&nbsp;&nbsp;&nbsp;<i class='bx bx-link' style="color:#CE8719;" ></i>
                                                <a href="{{'mailto:'.$data->gmail_link}}" style="color:#212529; text-decoration:none;" target="_blank">{{$data->gmail_link}}</a>
                                            </span>
                                        </div>
                                    
                                        <div class="btn-wrapper">
                                            <button class="but mx-2 "><i class='bx bx-pencil bx-tada-hover bx-border-circle' style="font-size: 22px;" data-bs-toggle="modal" data-bs-target="#edit-gmail"></i></button>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <!-- End Social Links -->
           
                            <!-- Logo -->
                        <div class="home-banner-container">
                            <div class="section-heading">
                                <span>Branding</span>
                            </div>
                            <div class="d-flex justify-content-center  px-4 flex-wrap ">
                                    @foreach ($logo as $data)
                                    <div class="card" style="width: 15rem;">
                                        
                                        <div class="card-body d-flex justify-content-center">
                                            <img class="card-img" src="{{ url ('/images/' . $data->logo_img)}}">
                                            
                                        </div>
                                        
                                        <div class="card-footer d-flex justify-content-center">
                                            <button class="but mx-2"><i class='bx bx-pencil bx-tada-hover bx-border-circle' data-bs-toggle="modal" data-bs-target="#edit-logo" style="font-size: 22px;"></i></button>
                                        </div>
                                            
                                    </div>
                                    @endforeach
                            </div>
                        </div>
                            <!-- End Logo -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FB Edit -->
        @foreach ($social as $data)
        <div class="modal fade" id="edit-fb" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Facebook Link</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mx-3">

                        <form action="/edit-fb" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mt-3 ">
                                <label class="form-label">Social Platform</label>
                                <input type="text" class="form-control" name="name" value="Facebook" required disabled>
                            </div>

                            <div class="form-group mt-3  mb-3">
                                <label class="form-label">Link</label>
                                <input type="text" class="form-control" name="link" value="{{$data->fb_link}}" required>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                        <button type="submit"  class="btn px-3 modal-btn">Save</button>
                </div>
                    </form>
            </div>
        </div>
        </div>
        @endforeach
       <!-- End FB Edit -->

       <!-- Gmail Edit -->
       @foreach ($social as $data)
        <div class="modal fade" id="edit-gmail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Email Address</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mx-3">

                        <form action="/edit-gmail" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mt-3 ">
                                <label class="form-label">Social Platform</label>
                                <input type="text" class="form-control" name="name" value="Gmail" required disabled>
                            </div>

                            <div class="form-group mt-3  mb-3">
                                <label class="form-label">Link</label>
                                <input type="text" class="form-control" name="link" value="{{$data->gmail_link}}" required>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                        <button type="submit"  class="btn px-3 modal-btn">Save</button>
                </div>
                    </form>
            </div>
        </div>
        </div>
        @endforeach
       <!-- End Gmail Edit -->


       <!-- Gmail Edit -->
       @foreach ($social as $data)
        <div class="modal fade" id="edit-instagram" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Instagram Link</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mx-3">

                        <form action="/edit-instagram" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mt-3 ">
                                <label class="form-label">Social Platform</label>
                                <input type="text" class="form-control" name="name" value="Instagram" disabled required>
                            </div>

                            <div class="form-group mt-3  mb-3">
                                <label class="form-label">Link</label>
                                <input type="text" class="form-control" name="link" value="{{$data->instagram_link}}" required>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                        <button type="submit"  class="btn px-3 modal-btn">Save</button>
                </div>
                    </form>
            </div>
        </div>
        </div>
        @endforeach
       <!-- End Gmail Edit -->

       <!-- Logo Edit -->

       <div class="modal fade" id="edit-logo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Update Logo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mx-3">

                        <form action="/edit-logo" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mt-3 ">
                                <label class="form-label">Logo Image</label>
                                <input type="file" class="form-control" name="logo_img" required >
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button type="submit"  class="btn px-3 modal-btn">Save</button>
                </div>
                    </form>
            </div>
        </div>
        </div>
    </div>
    <script>
                               
        @if(Session::has('message'))
        toastr.options =
        {
            "closeButton" : false,
            "progressBar" : true,
            "positionClass": "toast-bottom-right",
            "timeOut": "2000",
        }
                toastr.success("{{ session('message') }}");
        @endif

        @if(Session::has('error'))
        toastr.options =
        {
            "closeButton" : false,
            "progressBar" : true,
            "positionClass": "toast-bottom-right",
            "timeOut": "2000",
        }
                toastr.error("{{ session('error') }}");
        @endif

    </script>
</body>
</html>