-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.24-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for ws102_etr
CREATE DATABASE IF NOT EXISTS `ws102_etr` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ws102_etr`;

-- Dumping structure for table ws102_etr.barkada_bundle
CREATE TABLE IF NOT EXISTS `barkada_bundle` (
  `bb_id` int(11) NOT NULL AUTO_INCREMENT,
  `bb_image` text NOT NULL,
  `bb_desc` varchar(150) NOT NULL DEFAULT '',
  `bb_price` varchar(100) NOT NULL DEFAULT '',
  `listing` int(11) NOT NULL DEFAULT 0,
  `stat` varchar(100) NOT NULL DEFAULT 'Are you sure you want to remove the item from the list?',
  `color` varchar(50) NOT NULL DEFAULT '#D98B19',
  PRIMARY KEY (`bb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.barkada_bundle: ~0 rows (approximately)
INSERT INTO `barkada_bundle` (`bb_id`, `bb_image`, `bb_desc`, `bb_price`, `listing`, `stat`, `color`) VALUES
	(13, '1655554105.jpg', 'Menu Description', 'Php 150.00', 1, 'Are you sure you want to add the item back to list?', 'gray');

-- Dumping structure for table ws102_etr.best_sellers
CREATE TABLE IF NOT EXISTS `best_sellers` (
  `bs_id` int(11) NOT NULL AUTO_INCREMENT,
  `bs_image` text NOT NULL,
  `bs_desc` varchar(150) NOT NULL,
  `bs_price` varchar(100) NOT NULL,
  `listing` int(11) NOT NULL DEFAULT 0,
  `stat` varchar(100) NOT NULL DEFAULT 'Are you sure you want to remove the item from the list?',
  `color` varchar(50) NOT NULL DEFAULT '#D98B19',
  PRIMARY KEY (`bs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.best_sellers: ~6 rows (approximately)
INSERT INTO `best_sellers` (`bs_id`, `bs_image`, `bs_desc`, `bs_price`, `listing`, `stat`, `color`) VALUES
	(20, '1655432572.jpg', 'Menu Description', 'Php 100.00', 1, 'Are you sure you want to add the item back to list?', 'gray'),
	(21, '1655432592.jpg', 'Menu Description', 'Php 100.00', 1, 'Are you sure you want to add the item back to list?', 'gray'),
	(22, '1655432612.png', 'Menu Description', 'Php 50.00', 1, 'Are you sure you want to add the item back to list?', 'gray'),
	(23, '1655432636.jpg', 'Menu Description', 'Php 20.00', 0, 'Are you sure you want to remove the item from the list?', '#D98B19'),
	(24, '1655527838.jpg', 'Menu Description', 'Php 100.00', 0, 'Are you sure you want to remove the item from the list?', '#D98B19'),
	(25, '1655550970.webp', 'Menu Description', 'Php 20.00', 1, 'Are you sure you want to add the item back to list?', 'gray');

-- Dumping structure for table ws102_etr.faqs
CREATE TABLE IF NOT EXISTS `faqs` (
  `f_id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY (`f_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.faqs: ~1 rows (approximately)
INSERT INTO `faqs` (`f_id`, `question`, `answer`) VALUES
	(14, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.', 'This is sample answer'),
	(15, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form?', 'This is sample answer.');

-- Dumping structure for table ws102_etr.home_page
CREATE TABLE IF NOT EXISTS `home_page` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_img` text NOT NULL,
  `heading` varchar(100) NOT NULL,
  `sub_heading` varchar(100) NOT NULL,
  `time` varchar(50) NOT NULL,
  `color` text NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.home_page: ~1 rows (approximately)
INSERT INTO `home_page` (`item_id`, `banner_img`, `heading`, `sub_heading`, `time`, `color`) VALUES
	(1, '1655543771.png', 'Eat.Drink.Love', '"Fresh Air, warm Lights and good Food."', 'Open 1:00 pm - 9:00pm', '#D98B19');

-- Dumping structure for table ws102_etr.logo
CREATE TABLE IF NOT EXISTS `logo` (
  `logo_id` int(11) NOT NULL AUTO_INCREMENT,
  `logo_img` text NOT NULL,
  PRIMARY KEY (`logo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.logo: ~0 rows (approximately)
INSERT INTO `logo` (`logo_id`, `logo_img`) VALUES
	(1, '1655432764.png');

-- Dumping structure for table ws102_etr.org
CREATE TABLE IF NOT EXISTS `org` (
  `org_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_img` text NOT NULL,
  `name` varchar(150) NOT NULL DEFAULT '',
  `position` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.org: ~2 rows (approximately)
INSERT INTO `org` (`org_id`, `org_img`, `name`, `position`) VALUES
	(15, '1655434022.jpg', 'This is Name Plate', 'Owner'),
	(16, '1655434085.webp', 'This is Name Plate', 'Co-Owner');

-- Dumping structure for table ws102_etr.social
CREATE TABLE IF NOT EXISTS `social` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_link` varchar(50) NOT NULL,
  `gmail_link` varchar(150) NOT NULL,
  `instagram_link` varchar(150) NOT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.social: ~2 rows (approximately)
INSERT INTO `social` (`s_id`, `fb_link`, `gmail_link`, `instagram_link`) VALUES
	(1, 'https://www.facebook.com/campsawings', 'janpatrickdelacruzurbano@gmail.com', 'https://www.instagram.com');

-- Dumping structure for table ws102_etr.story
CREATE TABLE IF NOT EXISTS `story` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `story` text NOT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.story: ~0 rows (approximately)
INSERT INTO `story` (`s_id`, `story`) VALUES
	(1, '<p>Simply put,<strong> Loremipsum</strong> is dummy text that occupies the space where the real content should be. If you are designing an online business such as a blog and you do not have content already, you use a lorem ipsum generator to create placeholder or dummy text to show you how the business will look once you add the real content..</p><p>Lorem ipsum is common with <strong>typesetting and printing businesses</strong>. The text did not start with the age of digital businesses as it has been used since 1500s or even earlier. The first use must have been in the creation of a specimen book. The creators wanted to know how the book will look with text on and that was when lorem ipsum started finding its place in the printing, typesetting, and digital world.</p><p>The text has not changed much <i><strong>since the 1500s</strong></i>. It has held on to its place through the typewriter period and is now used on <strong>digital work</strong>. The release of Letraset sheets that contained lorem ipsum was the start of the popularity of lorem ipsum. Today, a lorem ipsum generator has made it easier to generate the text with much ease.</p>');

-- Dumping structure for table ws102_etr.sulit_meals
CREATE TABLE IF NOT EXISTS `sulit_meals` (
  `sm_id` int(11) NOT NULL AUTO_INCREMENT,
  `sm_image` text NOT NULL,
  `sm_desc` varchar(150) NOT NULL,
  `sm_price` varchar(100) NOT NULL,
  `listing` int(11) NOT NULL DEFAULT 0,
  `stat` varchar(100) NOT NULL DEFAULT 'Are you sure you want to remove the item from the list?',
  `color` varchar(50) NOT NULL DEFAULT '#D98B19',
  PRIMARY KEY (`sm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.sulit_meals: ~3 rows (approximately)
INSERT INTO `sulit_meals` (`sm_id`, `sm_image`, `sm_desc`, `sm_price`, `listing`, `stat`, `color`) VALUES
	(8, '1655433368.jpg', 'Menu Description', 'Php 70.00', 0, 'Are you sure you want to remove the item from the list?', '#D98B19'),
	(9, '1655433387.jpg', 'Menu Description', 'Php 30.00', 0, 'Are you sure you want to remove the item from the list?', '#D98B19'),
	(10, '1655433420.png', 'Menu Description', 'Php 70.00', 0, 'Are you sure you want to remove the item from the list?', '#D98B19');

-- Dumping structure for table ws102_etr.user
CREATE TABLE IF NOT EXISTS `user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ws102_etr.user: ~0 rows (approximately)
INSERT INTO `user` (`uid`, `user_email`, `user_password`) VALUES
	(1, 'email@email.com', 'admin123');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
