<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\GetController;
use App\Http\Controllers\ValidationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('client.home');
});

Route::get('/login/admin',[GetController::class,
    'showAdminLogin'
]);

Route::post('/login/admin', [ValidationController::class,
    'loginValidate'
]);

Route::get('/admin/dashboard',[GetController::class,
    'adminHome'
]);

Route::post('/edit-banner', [PostController::class,
    'editBanner'
]);

Route::post('/add-bestsellers', [PostController::class,
    'addBestSellers'
]);

Route::post('/add-sulitmeals', [PostController::class,
    'addSulitMeals'
]);

Route::post('/add-barkada-bundle', [PostController::class,
    'addBarkadaBundle'
]);

Route::post('/add-faqs', [PostController::class,
    'addFaqs'
]);

Route::post('/add-org', [PostController::class,
    'addOrg'
]);

Route::post('/delete-bs-item', [PostController::class,
    'deleteBSItem'
]);

Route::post('/delete-sm-item', [PostController::class,
    'deleteSMItem'
]);

Route::post('/delete-bb-item', [PostController::class,
    'deleteBBItem'
]);

Route::post('/delete-faqs-item', [PostController::class,
    'deleteFaqsItem'
]);

Route::post('/delete-org-item', [PostController::class,
    'deleteOrgItem'
]);


Route::get('/admin/menu',[GetController::class,
    'adminMenu'
]);

Route::get('/admin/faqs',[GetController::class,
    'adminFaqs'
]);

Route::get('/admin/about',[GetController::class,
    'adminAbout'
]);

Route::get('/admin/settings',[GetController::class,
    'adminSettings'
]);

Route::post('/update-story', [PostController::class,
    'updateStory'
]);

Route::post('/update-password', [ValidationController::class,
    'updatePassword'
]);

Route::post('/edit-fb', [PostController::class,
    'editFB'
]);

Route::post('/edit-gmail', [PostController::class,
    'editGmail'
]);

Route::post('/edit-instagram', [PostController::class,
    'editInstagram'
]);

Route::post('/edit-logo', [PostController::class,
    'editLogo'
]);

Route::get('/home',[GetController::class,
    'Home'
]);

Route::get('/',[GetController::class,
    'Home'
]);

Route::get('/menu',[GetController::class,
    'Menu'
]);

Route::get('/about',[GetController::class,
    'About'
]);

Route::get('/faqs',[GetController::class,
    'Faqs'
]);

Route::post('/enlist-item', [PostController::class,
    'enlistBSItem'
]);

Route::post('/enlist-sm', [PostController::class,
    'enlistSMItem'
]);

Route::post('/enlist-bb', [PostController::class,
    'enlistBBItem'
]);

