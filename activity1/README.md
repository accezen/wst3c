## Activity 1 First Laravel Blade Code

1. Add **activity1.blade.php** in Laravel project folder **/resources/views**.
2. Add or overwrite the existing "web.php" file on Laravel project folder **/routes** directory.

---

**Note:**You can use **Blade** Folder for complete and updated files for **First Blade Code Activity**. 