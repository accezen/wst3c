<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/',function(){
    return view('welcome');
});

Route :: get ('/customer/{id}/{name}/{add}/{age?}', function($id, $name, $add, $age = null){
    return "Name: Jan Patrick D.C Urbano<br>Section: BSIT 3C" . 
    "<br><br>ID: " . $id . 
    "<br>Name:" . $name . 
    "<br>Address: " . $add . 
    "<br>Age(Optional): " . $age;   
});

Route :: get ('/item/{itemNo}/{name}/{price}', function($itemNo, $name, $price){
    return "Name: Jan Patrick D.C Urbano<br>Section: BSIT 3C" . 
    "<br><br>Item No: " . $itemNo . 
    "<br>Name:" . $name . 
    "<br>Price: " . $price;
});

Route :: get ('/order/{cid}/{name}/{ordNo}/{date}', function($cid, $name, $ordNo, $date){
    return "Name: Jan Patrick D.C Urbano<br>Section: BSIT 3C" . 
    "<br><br>Customer ID: " . $cid . 
    "<br>Name:" . $name . 
    "<br>Order Number: " . $ordNo . 
    "<br>Date: " . $date;   
});

Route :: get ('/order-details/{transno}/{ordNo}/{itemID}/{name}/{price}/{qty}/{recNo?}', function($transno,$ordNo, $itemId, $name, $price, $qty, $recNo=null){
    $total = $price * $qty;
    return "Name: Jan Patrick D.C Urbano<br>Section: BSIT 3C" . 
    "<br><br>Transaction No: " . $transno . 
    "<br>Order No:" . $ordNo . 
    "<br>Item ID: " . $itemId . 
    "<br>Name: " . $name . 
    "<br>Price: " . $price . 
    "<br>Quantity: " . $qty .  
    "<br>Reciept No: " . $recNo . 
    "<br>Total: " . $total;   
});
 
 
// Route ::view('k','welcome');

// Route::get('/item',function(){
//     return view('item');
// });

// Route::get('/customer',function(){
//     return view('customer');
// });

// Route::get('/order',function(){
//     return view('order');
// });

// Route::get('/details',function(){
//     return view('details');
// });

// Route :: permanentRedirect('apple', 'ball');

// // Route :: get('ball', function(){
// //     return "redirected route";
// // });

// Route :: get('homepage', [HomeController::class, 'index']);